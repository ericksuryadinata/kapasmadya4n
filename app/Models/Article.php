<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['name', 'banner', 'status', 'description', 'slug'];

    public function scopeActive($query)
    {
    	return $query->where('status', 1);
    }

    public function image()
    {
    	return $this->hasMany(ArticleImage::class, 'article_id', 'id');
    }

    public function showBanner()
    {
    	if ($this->banner != null) {
    		return asset("storage/". $this->banner);
    	}
        return;
    }
}
