<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Storage;

class Gallery extends Model
{
    protected $fillable = ['name', 'status', 'slug', 'image', 'description'];

    public function showImage()
    {
        if ($this->image != null or Storage::exists($this->image)) {
            return asset('storage/' . $this->image);
        } else {
            return asset('assets/images/default.jpg');
        }
    }

    public function scopeActive($query)
    {
        $query->where('status', 1);
    }

    public function images()
    {
        return $this->hasMany('App\Models\GalleryImage');
    }
}
