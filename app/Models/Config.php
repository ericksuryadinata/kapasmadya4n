<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Config extends Model
{
    protected $fillable = ['name', 'logo', 'logo_white', 'icon', 'phone', 'email', 'fax', 'address', 'description', 'gmap_loc', 'koordinate'];

    public function getLatitude()
    {
    	$data = explode("|", $this->koordinate);
    	return $data[0];
    }

    public function getLongtitude()
    {
    	$data = explode('|', $this->koordinate);
    	return $data[1];
    }

    public function showLogo()
    {
        if ($this->logo != null or Storage::exists($this->logo)) {
            return asset('storage/' . $this->logo);
        } else {
            return asset('assets/images/default-logo.png');
        }
    }

    public function showIcon()
    {
        if ($this->icon != null or Storage::exists($this->icon)) {
            return asset('storage/' . $this->icon);
        } else {
            return asset('assets/images/default-icon.png');
        }
    }
}
