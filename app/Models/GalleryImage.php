<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
    protected $fillable = ['image', 'gallery_id'];

    public function show()
    {
        return asset('storage/'.  $this->image);
    }
}
