<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $fillable = ['title', 'keyword', 'author', 'tagline', 'fb_pixel', 'analytic'];
}
