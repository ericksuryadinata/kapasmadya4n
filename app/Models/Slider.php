<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Slider extends Model
{
	protected $fillable = ['image', 'status'];

	public function showImage()
	{
		if ($this->image != null or Storage::exists($this->image)) {
			return asset('storage/'.$this->image);
		} else {
            return asset('assets/images/default-slider.jpg');
        }
	}
}
