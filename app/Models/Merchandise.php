<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Merchandise extends Model
{
	protected $fillable = ['name', 'image', 'description', 'price', 'whatsapp', 'status','slug'];

	public function showImage()
	{
		if ($this->image != null) {
			return asset('storage/'.$this->image);
		}
		return;
	}

	public function Harga()
	{
		return $this->Rupiah($this->price);
	}

    public function Whatsapp()
    {
    	$number = strlen($this->whatsapp);
    	$code = substr($this->whatsapp, 0,1);
    	$str = str_replace($code, '62', $code);
    	return $str.substr($this->whatsapp, 1,$number);
    }


	public function scopeActive()
	{
		return $this->where('status', 1);
	}

	public function getPriceItemAttribute() {
		return $this->Rupiah($this->price);
	}

    public function Rupiah($str)
    {
		$result = "Rp " . number_format($str,2,',','.');
		$res = explode(",", $result);
		return $res[0];
    }
}
