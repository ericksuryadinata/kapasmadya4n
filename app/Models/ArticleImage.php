<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleImage extends Model
{
	protected $fillable = ['image', 'article_id'];

	public function article()
	{
		return $this->belongsTo(Article::class);
	}

	public function showImage()
	{
		return asset('storage/'.$this->image);
	}
}
