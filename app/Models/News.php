<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['name', 'status', 'image', 'description', 'slug'];

    public function showImage()
    {
        if ($this->image != null or Storage::exists($this->image)) {
            return asset('storage/' . $this->image);
        } else {
            return asset('assets/images/default.jpg');
        }
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
