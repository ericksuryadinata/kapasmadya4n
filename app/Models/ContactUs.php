<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $fillable = ['name', 'avatar', 'position', 'description', 'whatsapp'];

    public function showImage()
    {
    	if ($this->avatar != null) {
    		return asset('storage/'.$this->avatar);
    	}
    	return;
    }

    public function getContactsAttribute()
    {
    	$number = strlen($this->whatsapp);
    	$code = substr($this->whatsapp, 0,1);
    	$str = str_replace($code, '62', $code);
    	return $str.substr($this->whatsapp, 1,$number);
    }
}
