<?php

namespace App\Http\Middleware;

use Closure;

class GlobalVariable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data['config'] = \App\Models\Config::first();
        $data['seo']    = \App\Models\Seo::first();
        $data['berita'] = \App\Models\News::first();
        $data['galeri'] = \App\Models\Gallery::first();
        $data['produk'] = \App\Models\Merchandise::first();
        \View::share($data);
        return $next($request);
    }
}
