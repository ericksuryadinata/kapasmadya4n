<?php

namespace App\Http\Controllers\Frontend\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;

class NewsController extends Controller
{
    public function index()
    {
        $data['newsFeed'] = News::orderBy('id','DESC')->paginate(6);
        $data['newsFeed'] = News::active()->orderBy('id','DESC')->paginate(6);
        return view('frontend.news.index',$data);
    }

    public function detail($slug)
    {
        $news = News::whereSlug($slug)->first();
        $postBefore = News::whereId('<', $news->id)->first();
        $postAfter = News::whereId('>', $news->id)->first();
        if ($postBefore == null) {
            $postBefore = 'none';
        }

        if ($postAfter == null) {
            $postAfter = 'none';
        }
        return view('frontend.news.detail', compact('news', 'postBefore', 'postAfter'));
    }
}
