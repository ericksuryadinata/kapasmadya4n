<?php

namespace App\Http\Controllers\Frontend\Information;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleImage;

class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['models'] = Article::orderBy('id', 'desc')->active()->paginate(10);
        $data['rescentPost'] = Article::orderBy('created_at', 'desc')->active()->take(3)->get();
        return view('frontend.information.index', $data);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['article'] = Article::where('id', $id)->first();
        $article = Article::where('id', $id)->first();
        $data['postAfter'] = Article::whereId('>', $id)->first();
        if ($data['postAfter'] == null) {
            $data['postAfter'] = 'none';
        }

        $data['postBefore'] = Article::whereId('<', $id)->first();
        if ($data['postBefore'] == null) {
            $data['postBefore'] = 'none';
        }

        if ($id == (int)9) {
            return redirect()->route('frontend.informasi.detail', 19);
        }
        // return dd($article); exit;
        $data['articleImage'] = ArticleImage::where('article_id', $id)->get();
        return view('frontend.information.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
