<?php

namespace App\Http\Controllers\Frontend\Merchandise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Merchandise;

class MerchandiseController extends Controller
{
    public function index()
    {
    	$data['merchandises'] = Merchandise::active()->get();
    	return view('frontend.merchandise.index', $data);
    }
}
