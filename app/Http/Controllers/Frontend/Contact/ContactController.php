<?php

namespace App\Http\Controllers\Frontend\Contact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;

class ContactController extends Controller
{
	public function index()
	{
	   return view('frontend.contact.index', ['models' => ContactUs::all()]);
	}
}
