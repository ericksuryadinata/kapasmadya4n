<?php

namespace App\Http\Controllers\Frontend\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\News;
use App\Models\Slider;

class HomeController extends Controller
{

    public function index()
    {
        $data['slider'] = Slider::whereStatus(1)->first();
        // it will prevent if there is no slider yet
        if($data['slider'] == null){
            $data['slider'] = new Slider();
        }
        $data['galleries'] = Gallery::whereStatus(1)->orderBy('id', 'desc')->take(6)->get();
        $data['newsFeed'] = News::active()->orderBy('id','desc')->take(6)->get();
    	return view('frontend.home.index', $data);
    }
}
