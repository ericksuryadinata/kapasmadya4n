<?php

namespace App\Http\Controllers\Frontend\Event;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\GalleryImage;

class EventController extends Controller
{
    public function index()
    {
        $data['galleries'] = Gallery::whereStatus(1)->orderBy('id', 'desc')->paginate(6);
    	return view('frontend.event.index',$data);
    }

    public function detail($slug){
        $gallery = Gallery::whereSlug($slug)->first();
        $postBefore = Gallery::whereId('<',$gallery->id)->first();
        $postAfter = Gallery::whereId('>',$gallery->id)->first();
        $galleryImage = GalleryImage::whereGalleryId($gallery->id)->get();
        if($postBefore == null){
            $postBefore = 'none';
        }

        if($postAfter == null){
            $postAfter = 'none';
        }
        return view('frontend.event.detail',compact('gallery','galleryImage','postBefore','postAfter'));
    }
}
