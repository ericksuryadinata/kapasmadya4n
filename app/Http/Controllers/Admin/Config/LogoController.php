<?php

namespace App\Http\Controllers\Admin\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config as Logo;
use Storage;

class LogoController extends Controller
{
    protected $logo;
    public function __construct()
    {
        $this->logo = new Logo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.config.logo');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Logo $logo)
    {
        return view('admin.config.logo', ['logo' => $logo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Logo $logo)
    {
        $request->validate(['logo' => 'image', 'logo_white' => 'image', 'icon' => 'image']);
        /*
         * KODINGAN INI BERISI BAD LOGIC
        */
        if ($request->hasFile('logo')) {
           if (Storage::exists($logo->logo)) {
                Storage::delete($logo->logo);
            }

            $field = [
                'logo' => $request->file('logo')->store('logo')
            ];
            $update = $logo->update($field);

        } elseif ($request->hasFile('logo_white')) {
            if (Storage::exists($logo->logo_white)) {
                Storage::delete($logo->logo_white);
            }

            $field = [
                'logo_white' => $request->file('logo_white')->store('logo')
            ];
            $update = $logo->update($field);
        
        } elseif ($request->hasFile('icon')) {
            if (Storage::exists($logo->icon)) {
                Storage::delete($logo->icon);
            }

            $field = [
                'icon' => $request->file('icon')->store('logo')
            ];
            $update = $logo->update($field);
        } else {
            Storage::delete($logo->logo);
            Storage::delete($logo->logo_white);
            Storage::delete($logo->icon);

            $field = [
                'logo' => $request->file('logo')->store('logo'),
                'logo_white' => $request->file('logo_white')->store('logo'),
                'icon' => $request->file('icon')->store('logo')
            ];

            $update = $logo->update($field);
        }

        if ($update) {
            return json_encode(['status' => true, 'response' => ['status' => 'success', 'msg' => 'Tentang website berhasil di perbarui', 'url' => route('admin.logo.edit', $logo->id)]]);
        }
        return json_encode(['status' => false, 'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
