<?php

namespace App\Http\Controllers\Admin\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Config $config)
    {
        return view('admin.config.website', ['config' => $config]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Config $config)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'gmap_loc' => 'required',
            'description' => 'required',
            'koordinate' => 'required',
        ]);

        if ($config->update($request->only('name', 'address', 'gmap_loc', 'description', 'koordinate'))) {

            // return redirect()->route('admin.config.edit', $config->id);
            return json_encode(['status' => true, 'response' => ['status' => 'success', 'msg' => 'Tentang website berhasil di perbarui', 'url' => route('admin.config.edit', $config->id)]]);
        }

        return json_encode(['status' => false, 'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
