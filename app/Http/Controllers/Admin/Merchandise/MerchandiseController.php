<?php

namespace App\Http\Controllers\Admin\Merchandise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Merchandise;
use Storage;

class MerchandiseController extends Controller
{
    protected $merchandise;
    public function __construct()
    {
        $this->merchandise = new Merchandise;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.merchandise.index', ['models' => Merchandise::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.merchandise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required|image',
            'description' => 'required',
            'price' => 'required',
            'whatsapp' => 'required',
        ]);

        $create = [
            'name' => $request->name,
            'image' => $request->file('image')->store('merchandise'),
            'description' => $request->description,
            'price' => $request->price,
            'whatsapp' => $request->whatsapp,
            'slug' => str_slug($request->name),
            'status' => $request->status ? true : false
        ];

        if ($this->merchandise->create($create)) {
            return json_encode([
                'status' => true,
                'response' => [
                    'status' => 'success',
                    'msg' => 'Barang merchandise berhasil di tambahkan',
                    'url' => route('admin.merchandise.index')
                ]
            ]);
        }

        return json_encode([
            'status' => false,
            'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Merchandise $merchandise)
    {
        return view('admin.merchandise.edit', ['model' => $merchandise]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Merchandise $merchandise)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'image',
            'description' => 'required',
            'price' => 'required',
            'whatsapp' => 'required',
        ]);

        if ($request->hasFile('image')) {

            if (Storage::exists($merchandise->image)) {
                Storage::delete($merchandise->image);
            }

            $fields = [
                'name' => $request->name,
                'image' => $request->file('image')->store('merchandise'),
                'description' => $request->description,
                'price' => $request->price,
                'whatsapp' => $request->whatsapp,
                'status' => $request->status ? true : false
            ];

            $update = $merchandise->update($fields);
        } else {
            $fields = [
                'name' => $request->name,
                'description' => $request->description,
                'price' => $request->price,
                'whatsapp' => $request->whatsapp,
                'status' => $request->status ? true : false
            ];

            $update = $merchandise->update($fields);
        }

        if ($update) {
            return json_encode([
                'status' => true,
                'response' => [
                    'status' => 'success',
                    'msg' => 'Barang merchandise berhasil di perbarui',
                    'url' => route('admin.merchandise.index')
                ]
            ]);
        }
        return json_encode([
            'status' => false,
            'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Merchandise $merchandise)
    {
        if (Storage::exists($merchandise->image)) {
            Storage::delete($merchandise->image);
        }

        if ($merchandise->delete()) {
            return redirect()->route('admin.merchandise.index')->with(['status' => 'success', 'msg' => 'Merchandise berhasil di hapus']);
        }
        return redirect()->route('admin.merchandise.index')->with(['status' => 'danger', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']);
    }
}
