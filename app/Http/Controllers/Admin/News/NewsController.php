<?php

namespace App\Http\Controllers\Admin\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;
use Storage;

class NewsController extends Controller
{
    protected $news;
    public function __construct()
    {
        $this->news = new News;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.news.index', ['models' => News::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required|image',
            'description' => 'required',
        ]);

        $create = [
            'name' => $request->name,
            'image' => $request->file('image')->store('News'),
            'description' => $request->description,
            'slug' => str_slug($request->name),
            'status' => $request->status ? true : false
        ];

        if ($this->news->create($create)) {
            return json_encode([
                'status' => true,
                'response' => [
                    'status' => 'success',
                    'msg' => 'News berhasil di tambahkan',
                    'url' => route('admin.news.index')
                ]
            ]);
        }

        return json_encode([
            'status' => false,
            'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        return view('admin.news.edit', ['news' => $news]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'image',
            'description' => 'required',
        ]);

        if ($request->hasFile('image')) {

            if (Storage::exists($news->image)) {
                Storage::delete($news->image);
            }

            $fields = [
                'name' => $request->name,
                'image' => $request->file('image')->store('News'),
                'description' => $request->description,
                'status' => $request->status ? true : false
            ];

            $update = $news->update($fields);
        } else {
            $fields = [
                'name' => $request->name,
                'description' => $request->description,
                'status' => $request->status ? true : false
            ];

            $update = $news->update($fields);
        }

        if ($update) {
            return json_encode([
                'status' => true,
                'response' => [
                    'status' => 'success',
                    'msg' => 'News berhasil di perbarui',
                    'url' => route('admin.news.index')
                ]
            ]);
        }
        return json_encode([
            'status' => false,
            'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        if (Storage::exists($news->image)) {
            Storage::delete($news->image);
        }

        if ($news->delete()) {
            return redirect()->route('admin.news.index')->with(['status' => 'success', 'msg' => 'News berhasil di hapus']);
        }
        return redirect()->route('admin.news.index')->with(['status' => 'danger', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']);
    }
}
