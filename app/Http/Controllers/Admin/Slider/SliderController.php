<?php

namespace App\Http\Controllers\Admin\Slider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use Storage;

class SliderController extends Controller
{
    protected $slider;
    public function __construct()
    {
        $this->slider = new Slider;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.slider.index', ['models' => Slider::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image'
        ]);

        $create = [
            'image' => $request->file('image')->store('slider'),
            'status' => $request->status ? true : false
        ];

        if ($this->slider->create($create)) {
            return json_encode([
                'status' => true,
                'response' => [
                    'status' => 'success',
                    'msg' => 'Slider berhasil di tambahkan',
                    'url' => route('admin.slider.index')
                ]
            ]);
        }
        return json_encode([
            'status' => false,
            'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('admin.slider.edit', ['model' => $slider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $request->validate([
            'image' => 'image'
        ]);

        if ($request->hasFile('image')) {
            if ($slider->image != null) {
                Storage::delete($slider->image);
            }

            $field = [
                'image' => $request->file('image')->store('slider'),
                'status' => $request->status ? true : false
            ];

            $update = $slider->update($field);
        } else {
            $status = $request->status ? true : false;
            $update = $slider->update(['status' => $status]);
        }

        if ($update) {
            return json_encode([
                'status' => true,
                'response' => [
                    'status' => 'success',
                    'msg' => 'Slider berhasil di perbarui',
                    'url' => route('admin.slider.index')
                ]
            ]);
        }
        return json_encode([
            'status' => false,
            'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        if ($slider->image != null or Storage::exists($slider->image)) {
            Storage::delete($slider->image);
        }

        if ($slider->delete()) {
            return redirect()->route('admin.slider.index')->with(['status' => 'success', 'msg' => 'Slider berhasil di hapus']);
        }
        return redirect()->route('admin.slider.index')->with(['status' => 'danger', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']);
    }
}
