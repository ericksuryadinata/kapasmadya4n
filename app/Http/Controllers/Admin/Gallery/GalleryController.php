<?php

namespace App\Http\Controllers\Admin\Gallery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\GalleryImage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
    protected $gallery;
    protected $imageGallery;
    public function __construct()
    {
        $this->gallery = new Gallery;
        $this->imageGallery = new GalleryImage;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->forget('admin' . auth()->user()->id . '.gallery.images');
        $request->session()->forget('admin' . auth()->user()->id . '.gallery.delete.images');
        return view('admin.gallery.index', ['models' => Gallery::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required', 'description' => 'required']);

        $create = [
            'name' => $request->name,
            'description' => $request->description,
            'status' => ($request->status) ? true : false,
            'type' => ($request->type) ? 'image' : 'video',
            'slug' => str_slug($request->name),
            'image' => $request->file('image')->store('gallery'),
        ];


        if ($gallery = Gallery::create($create)) {
            // after we save the gallery get the id
            // destination path
            $destinationPath = 'files/gallery/';
            $sourcePath = 'tmp/gallery/';
            // get the gallery image name from sessions
            $imagesFromSession = $request->session()->get('admin' . auth()->user()->id . '.gallery.images');
            if ($imagesFromSession) {
                // let we save the image path to database, and move the image to right destination
                foreach ($imagesFromSession as $key => $image) {
                    // old file path
                    $oldPath = $sourcePath . $image->name;
                    // new file path
                    $newPath = $destinationPath . $image->name;
                    // save the last inserted id
                    $galleryImage = new GalleryImage();
                    $galleryImage->gallery_id = $gallery->id;
                    // save new path for the gallery image
                    $galleryImage->image = $newPath;
                    $galleryImage->save();
                    // move old file to new file
                    Storage::move($oldPath, $newPath);
                }
            }
            return json_encode(['status' => true, 'response' => ['status' => 'success', 'msg' => 'Data gallery berhasil di tambahkan', 'url' => route('admin.gallery.index')]]);
        }
        return json_encode(['status' => false, 'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, ulangi beberapa saat lagi']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show($gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$gallery)
    {
        $request->session()->forget('admin' . auth()->user()->id . '.gallery.images');
        $request->session()->forget('admin' . auth()->user()->id . '.gallery.delete.images');
        // take the image
        $galleryImage = GalleryImage::whereGalleryId($gallery)->get();
        // let's build the sessions
        foreach ($galleryImage as $key => $image) {
            $path = $image->image;
            // take the key
            $key = explode('KGTNWRG' . auth()->user()->id . '-', $path);
            // the filename
            $file = explode('files/gallery/', $path);
            $sessionsImage = (object) ['name' => $file[1], 'key' => $key[1], 'option' => 'edit', 'id' => $image->id];
            $request->session()->push('admin' . auth()->user()->id . '.gallery.images', $sessionsImage);
        }
        $galleries = Gallery::findOrFail($gallery);
        return view('admin.gallery.edit', compact('galleries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $gallery)
    {
        $request->validate(['name' => 'required', 'description' => 'required']);
        $galleries = Gallery::findOrFail($gallery);

        if ($request->hasFile('image')) {

            if (Storage::exists($galleries->image)) {
                Storage::delete($galleries->image);
            }

            $fields = [
                'name' => $request->name,
                'description' => $request->description,
                'status' => ($request->status) ? true : false,
                'type' => ($request->type) ? 'image' : 'video',
                'slug' => str_slug($request->name),
                'image' => $request->file('image')->store('gallery'),
            ];

            $update = $galleries->update($fields);
        } else {
            $fields = [
                'name' => $request->name,
                'description' => $request->description,
                'status' => ($request->status) ? true : false,
                'type' => ($request->type) ? 'image' : 'video',
                'slug' => str_slug($request->name),
            ];

            $update = $galleries->update($fields);
        }

        if ($update) {
            // destination path

            $destinationPath = 'files/gallery/';
            $sourcePath = 'tmp/gallery/';
            // get the gallery image name from sessions
            $imagesFromSession = $request->session()->get('admin' . auth()->user()->id . '.gallery.images');
            if ($imagesFromSession) {
                // let we save the image path to database, and move the image to right destination
                foreach ($imagesFromSession as $key => $image) {
                    // only process if the option is create
                    if ($image->option == 'create') {
                        // old file path
                        $oldPath = $sourcePath . $image->name;
                        // new file path
                        $newPath = $destinationPath . $image->name;
                        // save the last inserted id
                        $galleryImage = new GalleryImage();
                        $galleryImage->gallery_id = $galleries->id;
                        // save new path for the gallery image
                        $galleryImage->image = $newPath;
                        $galleryImage->save();
                        // move old file to new file
                        Storage::move($oldPath, $newPath);
                    }
                }
            }

            // now we process the deletion image from sessions we have, it will remove the record from database too
            $imagesDeleteSession = $request->session()->get('admin' . auth()->user()->id . '.gallery.delete.images');
            if ($imagesDeleteSession) {
                // let we save the image path to database, and move the image to right destination
                foreach ($imagesDeleteSession as $key => $image) {
                    // delete the images
                    $path = public_path('storage/files/gallery/' . $image->delete);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                    // delete from database too
                    $galleryImage = GalleryImage::findOrFail($image->id);
                    $galleryImage->delete();
                }
            }
            return json_encode([
                'status' => true,
                'response' => [
                    'status' => 'success',
                    'msg' => 'Kegiatan warga berhasil di perbarui',
                    'url' => route('admin.gallery.index')
                ]
            ]);
        }
        return json_encode([
            'status' => false,
            'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy($gallery)
    {
        $decrypted = $gallery;
        $gallery = Gallery::findOrFail($decrypted);
        $galleryImage = GalleryImage::whereGalleryId($gallery->id)->get();
        foreach ($galleryImage as $key => $image) {
            // delete the images
            $path = public_path('storage/' . $image->image);
            if (file_exists($path)) {
                unlink($path);
            }
            $galleryImageDelete = GalleryImage::findOrFail($image->id);
            $galleryImageDelete->delete();
        }

        if ($gallery->delete()) {
            return redirect()->route('admin.gallery.index')->with(['status' => 'success', 'msg' => 'Kegiatan warga berhasil di hapus']);
        }
        return redirect()->route('admin.gallery.index')->with(['status' => 'danger', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']);
    }

    /**
     * Store only gallery images that uploaded by admin
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeImages(Request $request)
    {

        $file = $request->file('file');
        $unique = uniqid();
        $fileName = str_slug(\explode(".", $file->getClientOriginalName())[0]) .
            "-KGTNWRG" . auth()->user()->id . "-" . $unique . "." . $file->extension();
        $response_success = (object) ['name' => $fileName, 'key' => $unique, 'option' => 'create', 'id' => $unique];
        $request->session()->push('admin' . auth()->user()->id . '.gallery.images', $response_success);
        // if success, move the image to tmp first, because admin can cancel to upload
        $upload_success = $file->storeAs("tmp/gallery", $fileName);

        if ($upload_success) {
            return response()->json($response_success, 200);
        } else {
            return response()->json('error', 400);
        }
    }

    /**
     * Delete only gallery images that uploaded by admin
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function deleteImages(Request $request)
    {
        // get the filekey, which is key from what we pass from xhr post
        $filekey =  $request->filekey;
        // take the sessions
        $sessionsImage = $request->session()->pull('admin' . auth()->user()->id . '.gallery.images', (object) []);
        // we will bruteforce checking whether it's filekey exists in sessions
        foreach ($sessionsImage as $key => $value) {
            // compare the key with filekey
            if ($value->key == $filekey) {
                // store to deleted item
                $itemDelete = $value->name;
                // we need this to check if method is edit or create a new gallery
                $option = $value->option;
                // take the id, if from database it will return the ID,
                // if a new gallery it will return uniqueid which is later will be useless
                $imageId = $value->id;
                // unset the sessions
                unset($sessionsImage[$key]);
            }
        }
        // we store remaining sessions
        $sessionsImage = $request->session()->put(
            'admin' . auth()->user()->id . '.gallery.images',
            array_values($sessionsImage)
        );
        // check if option is edit
        if ($option == 'edit') {
            // we will need another sessions for update, it contains the deleted image and the id from database
            $path = public_path('storage/files/gallery/' . $itemDelete);
            $deleted = (object) ['delete' => $itemDelete, 'id' => $imageId];
            $request->session()->push('admin' . auth()->user()->id . '.gallery.delete.images', $deleted);
        } else {
            $path = public_path('storage/tmp/gallery/' . $itemDelete);
            if (file_exists($path)) {
                unlink($path);
            }
        }

        if ($path) {
            return response()->json($itemDelete, 200);
        } else {
            return response()->json('error', 400);
        }
    }


    /**
     * Get images that uploaded by admin
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getImages(Request $request)
    {
        // if no request yet
        if (isset($request->id)) {
            $decrypted = $request->id;
            $images = [];
            $galleryImage = GalleryImage::whereGalleryId($decrypted)->get();
            // let's build the image parameters
            foreach ($galleryImage as $key => $image) {
                $path = $image->image;
                $file = explode('files/gallery/', $path);
                $key = explode('KGTNWRG' . auth()->user()->id . '-', $path);
                $images[] = [
                    'original' => $file[1],
                    'server' => $path,
                    'size' => File::size(public_path('storage/' . $path)),
                    'name' => $file[1],
                    'key' => $key[1]
                ];
            }
        } else {
            // this handle when user accidentally break the required field, so we need bring back the images
            $images = [];
            $imagesFromSession = $request->session()->get('admin' . auth()->user()->id . '.gallery.images');
            if ($imagesFromSession) {
                foreach ($imagesFromSession as $key => $image) {
                    $path = $image->name;
                    $file = 'tmp/gallery/' . $path;
                    $key = explode('KGTNWRG' . auth()->user()->id . '-', $path);
                    $key = explode('.', $key[1]);
                    $images[] = [
                        'original' => $path,
                        'server' => $file,
                        'size' => File::size(public_path('storage/' . $file)),
                        'name' => $path,
                        'key' => $key[0]
                    ];
                }
            }
        }

        return response()->json(['images' => $images]);
    }
}
