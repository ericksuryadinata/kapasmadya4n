<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\ArticleImage;
use Str;
use File;
use Storage;

class ArticleController extends Controller
{
    protected $article;
    protected $articleImage;
    public function __construct()
    {
        $this->article      = new Article;
        $this->articleImage = new ArticleImage;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->forget('admin'.auth()->user()->id.'.article_image');
        $request->session()->forget('admin'.auth()->user()->id.'.article_image_delete');
        $data['models'] = Article::all();
        return view('admin.article.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // if ($request->session()->get('admin'.auth()->user()->id.'.image_product') == null) {
        //     $file = glob('storage/tmp/product/*');
        //     foreach ($file as $key => $value) {
        //         if (is_file($value)) {
        //             unlink("{$value}");
        //         }
        //     }
        // }
        return view('admin.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'banner' => 'required'
        ]);

        $image = $request->file('banner');
        $originalName = $image->getClientOriginalName();
        $extFile = $image->getClientOriginalExtension();

        $create = [
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'description' => $request->description,
            'banner' => $image->storeAs('article', ucwords(Str::slug($request->name)).'.'.$extFile),
            'status' => $request->status ? true : false
        ];

        if ($article = $this->article->create($create)) {
            // setelah menyimpan data product kemudian ambil id product

            $destinationPath = 'image/article/';
            $sourcePath = 'tmp/article/';

            //Ambil data image pada session
            $imageFromSession = $request->session()->get('admin'.auth()->user()->id.'.article_image');
            if ($imageFromSession) {
                // jika datanya ada maka jadikan string
                foreach ($imageFromSession as $key => $image) {
                    // old path & name image product
                    $oldPath = $sourcePath. $image->name;
                    // new path & name image product
                    $newPath = $destinationPath.$image->name;

                    $articleImage               = new ArticleImage;
                    $articleImage->article_id   = $article->id;
                    $articleImage->image        = $newPath;
                    $articleImage->save();

                    Storage::move($oldPath, $newPath);
                }
            }

            return json_encode(['status' => true, 'response' => ['status' => 'Success', 'msg' => 'Data product berhasil di tambahkan', 'url' => route('admin.article.index')]]);
        }
        return json_encode(['status' => false, 'response' => ['status' => 'Error', 'Terjadi kesalahan, harap ulangi beberapa saat lagi!']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Article $article)
    {
        $request->session()->forget('admin'.auth()->user()->id.'.article_image');
        $request->session()->forget('admin'.auth()->user()->id.'.article_image_delete');

        $articleImage = ArticleImage::where('article_id', $article->id)->get();
        foreach ($articleImage as $key => $data) {
            $path = $data->image;
            $key = explode('article'.auth()->user()->id.'-', $path);
            $file = explode('image/article', $path);
            $saveImageToSession = (object)['name' => $file[1], 'key' => $key[1], 'option' => 'edit', 'id' => $data->id];
            $request->session()->push('admin'.auth()->user()->id.'.article_image', $saveImageToSession);
        }
        // dd(session()->get('admin'.auth()->user()->id.'.article_image_delete')); exit;
        $data['model'] = $article;
        return view('admin.article.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $request->validate([
            'name'          => 'required',
            'description'   => 'required',
        ]);

        if ($request->hasFile('banner')) {

            if (Storage::exists($article->banner)) {
                Storage::delete($article->banner);
            }

            $fields = [
                'name' => $request->name,
                'slug' => Str::slug($request->name),
                'description' => $request->description,
                'banner' => $request->file('banner')->storeAs('article', Str::slug($request->name).'.'.$request->file('banner')->getClientOriginalExtension()),
                'status' => $request->status ? true : false
            ];
            $update = $article->update($fields);
        } else {
            $fields = [
                'name' => $request->name,
                'slug' => Str::slug($request->name),
                'description' => $request->description,
                'status' => $request->status ? true : false
            ];

            $update = $article->update($fields);
        }

        if ($update) {
            $destinationPath = 'image/article';
            $sourcePath = 'tmp/article';

            $getImageFromSession = $request->session()->get('admin'.auth()->user()->id.'.article_image');
            if ($getImageFromSession) {

                foreach ($getImageFromSession as $key => $data) {
                    if ($data->option == 'create') {
                        $oldPath = $sourcePath. $data->image;
                        $newPath = $destinationPath. $data->image;

                        $articleImage = new ArticleImage;
                        $articleImage->article_id = $article->id;
                        $articleImage->image = $newPath;
                        $articleImage->save();

                        Storage::move($oldPath, $newPath);
                    }
                }
            }

            $deleteImageFromSession = $request->session()->get('admin' . auth()->user()->id . '.article_image_delete');
            if (deleteImageFromSession) {
                foreach ($deleteImageFromSession as $key => $image) {
                    $path = public_path('storage/image/article/'. $image->delete);
                    if (file_exists($path)) {
                        unlink($path);
                    }

                    $articleImage = ArticleImage::findOrFail($image->id);
                    $articleImage->delete();
                }
            }

            return json_encode(['status' => true, 'response' => ['status' => 'Success', 'msg' => 'Informasi berhasil di tambahakan', 'url' => route('admin.article.index')]]);
        }

        return json_encode(['status' => false, 'response' => ['status' => 'Error', 'msg' => 'Telah terjadi kesalahan, harap ulangi beberapa saat lagi']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $articleImages = ArticleImage::where('article_id', $article->id)->get();
        foreach ($articleImages as $key => $articleImage) {
            $path = public_path('storage/'.$articleImage->image);
            if (file_exists($path)) {
                unlink($path);
            }

            $image = ArticleImage::findOrFail($articleImage->id);
            $image->delete();
        }

        if (Storage::exists($article->banner)) {
            Storage::delete($article->banner);
        }

        if ($article->delete()) {
            return redirect()->route('admin.article.index')->with(['status' => 'success', 'msg' => 'Informasi berhasil di hapus']);
        }
        return json_encode(['status' => false, 'response' => ['status' => 'Error', 'msg' => 'Telah terjadi kesalahan, harap ulangi beberapa saat lagi']]);
    }
}
