<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArticleImage;
use Str;
use File;

class ImageController extends Controller
{

	public function storeImage(Request $request)
	{
		// default name input dropzone is file
		$file 		= $request->file('file');
		$uniq 		= uniqid();
		$name 		= explode('.', $file->getClientOriginalName());
		$ext 		= $name[1];
		$fileName 	= Str::slug($name[0].'-article'.auth()->user()->id.'-'.$uniq).'.'.$name[1];
		$response 	= (object) ['name' => $fileName, 'key' => $uniq, 'option' => 'create', 'id' => $uniq];
		$request->session()->push('admin'.auth()->user()->id.'.article_image', $response);
		$successUpload = $file->storeAs('tmp/article', $fileName);

		if ($successUpload) {
			return response()->json($response, 200);
		} else {
			return response()->json('error', 400);
		}
	}

	public function getImage(Request $request)
	{
		// jika tidak ada request id
		if (isset($request->id)) {
			$id = $request->id;
			$image = [];
			$articleImage = ArticleImage::where('article_id', $id)->get();

			foreach ($articleImage as $key => $data) {
				$fullPath 		= $data->image;
				$fileLocation 	= 'tmp/article/'. $fullPath;
				$getFileName 	= explode('image/article/', $fullPath);
				$getKey 		= explode('-article'.auth()->user()->id.'-', $fullPath);
				$key 			= explode('.', $getKey[1]);
				$image[] = [
					'original' => $fullPath,
					'server' => $fileLocation,
					'size' => File::size(public_path('storage/'.$fullPath)),
					'name' => $fullPath,
					'key' => $key[0]
				];
			}
		} else {
			$image = [];
			// ambil data pada session
			$getImageFromSession = $request->session()->get('admin'.auth()->user()->id.'.article_image');
			if ($getImageFromSession) {
				foreach ($getImageFromSession as $key => $image) {
					$fullPath = $image->name;
					$fileLocation = 'tmp/article/'.$fullPath;
					$getKey = explode('article'.auth()->user()->id.'-', $fullPath);
					$key = explode('.', $getKey[1]);
					$image[] = [
						'original' => $fullPath,
						'server' => $fileLocation,
						'size' => File::size(public_path('storage/'), $fileLocation),
						'name' => $fullPath,
						'key' => $key[0],
					];
				}
			}
		}

		return response()->json(['images' => $image]);
	}

	public function deleteImage(Request $request)
	{
		// ambil file_key
		$fileKey = $request->filekey;
		// ambil data image dari session
		$getImageFromSession = $request->session()->pull('admin'.auth()->user()->id.'.article_image', (object) []);
		// brute force 
		foreach ($getImageFromSession as $key => $data) {
			// ambil data key kemudian samakan dengan nilai file key
			if ($data->key == $fileKey) {
				// file yang akan di hapus
				$deleteFile = $data->name;
				// option sangat di perlukan untuk memastikan method edit atau buat baru
				$option = $data->option;
				$imageId = $data->id;
				// hapus session
				unset($getImageFromSession[$key]);
			}
		}

		$getImageFromSession = $request->session()->put('admin'.auth()->user()->id.'.article_image', array_values($getImageFromSession));
		// cek validasi, jika nilai dari variable option adalah edit
		if ($option == 'edit') {
			$path = public_path('storage/image/article/'. $deleteFile);
			$delete = (object)['delete' => $deleteFile, 'id' => $imageId];
			$request->session()->push('admin'.auth()->user()->id.'.article_image_delete', $delete);
		} else {
			$path = public_path('storage/tmp/article/'.$deleteFile);
			if (file_exists($path)) {
				unlink($path);
			}
		}

		if ($path) {
			return response()->json($deleteFile, 200);
		} else {
			return response()->json('error', 400);
		}
	}
}
