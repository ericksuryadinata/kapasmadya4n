<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class AuthController extends Controller
{
    public function index()
    {
    	return view('admin.auth.login');
    }

    public function store(Request $request)
    {
    	$request->validate(['username' => 'required', 'password' => 'required']);
    	$credentials = ['username' => $request->username, 'password' => $request->password];

    	if (Auth::attempt($credentials)) {
    		return json_encode(['status' => true, 'response' => ['status' => 'Aw, Yeah!', 'bg' => 'success', 'msg' => 'Login Sukses!', 'url' => route('admin.index')]]);
    	}
    	return json_encode(['status' => false, 'response' => ['status' => 'Aw, Snap!', 'bg' => 'danger', 'msg' => 'Pasword / Username Salah']]);
    }

    public function signout()
    {
    	auth()->guard('admin')->logout();
    	return redirect()->route('login.admin');
    }
}
