<?php

namespace App\Http\Controllers\Admin\ContactUs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use Storage;

class ContactUsController extends Controller
{
    protected $contact;
    public function __construct()
    {
        $this->contactus = new ContactUs;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.contactus.index', ['models' => ContactUs::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contactus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'avatar' => 'required|image',
            'position' => 'required',
            'description' => 'required',
            'whatsapp' => 'required'
        ]);

        $path = $request->file('avatar')->store('panitia');

        if ($this->contactus->create($request->only('name', 'position', 'description', 'whatsapp') + ['avatar' => $path])) {
            return json_encode([
                'status' => true,
                'response' => [
                    'status' => 'success',
                    'msg' => 'Kontak panitia berhasil di tambahkan',
                    'url' => route('admin.contactus.index')
                ]
            ]);
        }
        return json_encode([
            'status' => false,
            'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactUs $contactus)
    {
        return view('admin.contactus.edit', ['model' => $contactus]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactUs $contactus)
    {
        $request->validate([
            'name' => 'required',
            'avatar' => 'image',
            'position' => 'required',
            'description' => 'required',
            'whatsapp' => 'required'
        ]);

        if ($request->hasFile('avatar')) {
            
            if (Storage::exists($contactus->avatar)) {
                Storage::delete($contactus->avatar);
            }

            $path = $request->file('avatar')->store('panitia');
            $update = $contactus->update($request->only('name', 'position', 'description', 'whatsapp') + ['avatar' => $path]);
        } else {
            $update = $contactus->update($request->only('name', 'position', 'description', 'whatsapp'));
        }

        if ($update) {
            return json_encode([
                'status' => true,
                'response' => [
                    'status' => 'success',
                    'msg' => 'Kontak panitia berhasil di perbarui',
                    'url' => route('admin.contactus.index')
                ]
            ]);
        }
        return json_encode([
            'status' => false,
            'response' => ['status' => 'error', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactUs $contactus)
    {
        if (Storage::exists($contactus->avatar)) {
            Storage::delete($contactus->avatar);
        }

        if ($contactus->delete()) {
            return json_encode(['status' => true, 'response' => ['status' => 'success', 'msg' => 'Data panitia berhasil di hapus']]);
        }
        return json_encode(['status' => false, 'response' => ['status' => 'danger', 'msg' => 'Terjadi kesalahan, harap ulangi beberapa saat lagi']]);
    }
}
