
<!DOCTYPE html>
<html lang="en">

<head>
    @include('frontend.partials.header')
</head>

<body>
    <!-- Wrapper -->
    <div id="wrapper">
        @include('frontend.partials.navbar')
         <!-- Inspiro Slider -->
         @yield('heading')
        <!--end: Inspiro Slider -->

        <!-- Content -->
        <section id="page-content">
            @yield('content')
        </section>
        <!-- end: Content -->
        @yield('pop-up')
        <!-- Footer -->
        @include('frontend.partials.footer')
        <!-- end: Footer -->
    </div>
    <!-- end: Wrapper -->

    <!-- Go to top button -->
    <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a><!--Plugins-->
 <script src="{{ asset('assets/js/jquery.js') }}"></script>
 <script src="{{ asset('assets/js/plugins.js') }}"></script>

<!--Template functions-->
<script src="{{ asset('assets/js/functions.js') }}"></script> 
@yield('script')
</body>

</html>
