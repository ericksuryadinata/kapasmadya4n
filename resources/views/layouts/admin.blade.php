<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Administrator - @yield('title')</title>

	@include('admin.partials.style')

</head>

<body>

	<!-- Main navbar -->
	@include('admin.partials.navbar')
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">
			<!-- /main sidebar -->
			@include('admin.partials.sidebar')
			<!-- /main sidebar -->
			<!-- Main content -->
			<div class="content-wrapper">
				{{-- <div class="row"> --}}
					@yield('heading')
				{{-- </div> --}}
				<!-- Content area -->
				@include('admin.partials.content')
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
@include('admin.partials.script')
</body>
</html>
