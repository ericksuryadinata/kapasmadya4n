@extends('layouts.frontend')

@section('title', 'Berita - '. $news->name .' |')

@section('content')
<div class="container">
    <div class="row">
        <!-- content -->
        <div class="content col-md-12">
            <!-- Blog -->
            <div id="blog" class="single-post">
                <!-- Post single item-->
                <div class="post-item">
                    <div class="post-item-wrap">
                        <div class="post-image">
                            <img alt="{{ $news->name }}" src="{{ $news->showImage() }}">
                        </div>
                        <div class="post-item-description">
                            <h2>{{$news->name}}</h2>
                            <div class="post-meta">
                                <span class="post-meta-date"><i
                                        class="fa fa-calendar-o"></i>{{ $news->created_at->format('D, d-m-Y') }}</span>
                            </div>
                            <p>{!!$news->description!!}</p>
                        </div>

                        <div class="post-navigation">
                            @if ($postBefore !== 'none')
                            <a href="{{route('frontend.news.detail',['slug'=>$postBefore->slug])}}"
                                class="post-prev">
                                <div class="post-prev-title"><span>Previous Post</span>{{$postBefore->name}}
                                </div>
                            </a>
                            @endif
                            <a href="{{route('frontend.news')}}" class="post-all">
                                <i class="fa fa-th"></i>
                            </a>
                            @if ($postAfter !== 'none')
                            <a href="{{route('frontend.news.detail',['slug'=>$postAfter->slug])}}"
                                class="post-next">
                                <div class="post-next-title"><span>Next Post</span>{{$postAfter->name}}</div>
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- end: Post single item-->
            </div>
        </div>
        <!-- end: content -->
    </div>
</div>
@endsection
