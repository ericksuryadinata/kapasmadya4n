@extends('layouts.frontend')

@section('title', 'Berita |')

@section('content')
<div class="container">
    <div class="page-title text-center">
        <h1 class="p-b-10">Berita</h1>
    </div>
    <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
        @forelse ($newsFeed as $news)
        <div class="post-item border">
            <div class="post-item-wrap">
                <div class="post-image">
                    <a href="{{route('frontend.news.detail',["slug" => $news->slug])}}">
                        <img alt="{{ $news->name }}" src="{{ $news->showImage() }}">
                    </a>
                </div>
                <div class="post-item-description">
                    <span class="post-meta-date"><i class="fa fa-calendar-o"></i>{{ $news->created_at->format('D, d-m-Y') }}</span>
                    <h2><a href="{{route('frontend.news.detail',["slug" => $news->slug])}}">{{ $news->name }}</a>
                    </h2>
                </div>
            </div>
        </div>
        @empty
        <div class="text-center">
            <h3>Belum ada berita</h3>
        </div>
        @endforelse
    </div>
</div>

@endsection
