<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="author" content="{{ $config->author }}"/>
<meta name="description" content="{{ $config->description }}">
<!-- Document title -->
<title>@yield('title') {{$config->name}}</title>
<!-- Stylesheets & Fonts -->
<link rel="icon" href="{{ $config->showIcon() }}" sizes="32x32">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/plugins.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
@yield('style')
