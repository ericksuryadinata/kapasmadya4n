        <!-- Header -->
        <header id="header">
            <div id="header-wrap">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="#" class="logo" data-dark-logo="{{ $config->showLogo() }}">
                            <img style="height: 50px; vertical-align: middle;" src="{{ $config->showLogo() }}" alt="{{ $config->name }}">
                        </a>
                    </div>
                    <!--End: Logo-->

                    <!--Top Search Form-->
                    <div id="top-search">
                        <form action="search-results-page.html" method="get">
                            <input type="text" name="q" class="form-control" value="" placeholder="Start typing & press  &quot;Enter&quot;">
                        </form>
                    </div>
                    <!--end: Top Search Form-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="{{ route('frontend.home') }}">Beranda</a></li>
                                    <li><a href="{{ route('frontend.news') }}">Berita</a></li>
                                    <li><a href="{{ route('frontend.informasi') }}">Informasi</a></li>
                                    <li><a href="{{ route('frontend.activities') }}">Kegiatan warga</a></li>
                                    <li><a href="{{ route('frontend.merchandise') }}">Produk warga</a></li>
                                    <li><a href="{{ route('frontend.contact') }}">Kontak</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->
