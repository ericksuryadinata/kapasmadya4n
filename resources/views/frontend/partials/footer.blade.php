<!-- Footer -->
@php
    $position = 'relative';
    if($berita == null && Request::segment(1) === 'berita'){
        $position = 'fixed';
    }

    if($galeri == null && Request::segment(1) === 'kegiatan-warga'){
        $position = 'fixed';
    }

    if($produk == null && Request::segment(1) === 'produk-warga'){
        $position = 'fixed';
    }
@endphp
<footer id="footer" class="footer-light" style="z-index:999; border-top: transparent !important; position:{{$position}};bottom:0;left:0;width:100%;">
    <div class="footer-content"
        style="background-image: url({{ asset('assets/images/landmark.svg') }}) !important;">
    </div>
    <div class="copyright-content" style="background: #0670b9 !important; border-top: 1px solid #0670b9 !important">
        <div class="container">
        </div>
    </div>
</footer>
<!-- end: Footer -->
