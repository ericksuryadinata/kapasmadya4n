@extends('layouts.frontend')

@section('title')
Selamat datang di
@endsection

@section('heading')

<div id="slider" class="inspiro-slider slider-fullscreen arrows-large arrows-creative dots-creative" data-height-xs="360">
    <!-- Slide 1 -->
    <div class="slide kenburns" style="background-image:url('{{ $slider->showImage() }}');">
    </div>
    <div class="slide kenburns" style="background-image:url('{{ $slider->showImage() }}');">
    </div>
    <!-- end: Slide 1 -->
</div>

@endsection

@section('content')
<section id="section2" class="p-b-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-6" data-animate="fadeInUp" data-animate-delay="1000">
                <div class="m-b-40">
                    <h1 class="text-medium">WEBSITE KAMPUNG KAMI</h1>
                    <h1 class="text-small">BANK SAMPAH - SMART</h1>
                        <span class="lead">Video pertama bank sampah, yg diinisiasi oleh warga Kapas Madya 4N, kelurahan kapas madya baru, Surabaya.
                        Semoga hal yang sederhana ini membawa manfaat dan menbangun karakter bersih dan sehat untuk semuanya</span>
                    </div>
                {{-- <a href="#" class="btn btn-outline btn-dark"><span>Learn More</span></a> --}}
            </div>
            <div class="col-lg-6" style="height:300px" data-animate="fadeInLeft" data-animate-delay="800">
                <div class="m-b-40">
                    <iframe width="853" height="480" src="https://www.youtube.com/embed/lTYp5cXPr9g" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="p-b-20">
    <div class="container">
        <h1 class="text-center">KEGIATAN WARGA</h1>
        <div class="row">
            <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
                @forelse ($galleries as $gallery)
                    <div class="post-item border">
                        <div class="post-item-wrap fixed">
                            <div class="post-image">
                                <a href="{{route('frontend.activities.detail',["slug" => $gallery->slug])}}">
                                    <img alt="{{ $gallery->name }}" src="{{ $gallery->showImage() }}">
                                </a>
                            </div>
                            <div class="post-item-description">
                                <span class="post-meta-date"><i
                                        class="fa fa-calendar-o"></i>{{ $gallery->created_at->format('D, d-m-Y') }}</span>
                                <h2><a href="{{route('frontend.activities.detail',["slug" => $gallery->slug])}}">{{ $gallery->name }}</a></h2>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="text-center">
                        <h3>Belum ada kegiatan warga</h3>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</section>

<section class="{{count($galleries) >= 1 ? "p-t-0" : ""}}">
    <div class="container">
        <h1 class="text-center">BERITA WARGA</h1>
        <div class="row">
            <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
                @forelse ($newsFeed as $news)
                <div class="post-item border {{-- fixed --}}">
                    <div class="post-item-wrap fixed">
                        <div class="post-image">
                            <a href="{{route('frontend.news.detail',["slug" => $news->slug])}}">
                                <img alt="{{ $news->name }}" src="{{ $news->showImage() }}">
                            </a>
                        </div>
                        <div class="post-item-description">
                            <span class="post-meta-date"><i class="fa fa-calendar-o"></i>{{ $news->created_at->format('D, d-m-Y') }}</span>
                            <h2><a href="{{route('frontend.news.detail',["slug" => $news->slug])}}">{{ $news->name }}</a>
                            </h2>
                        </div>
                    </div>
                </div>
                @empty
                <div class="text-center">
                    <h3>Belum ada berita</h3>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</section>

@endsection

@section('style')
<style type="text/css">
    .fixed {
        min-height: 360px !important;
    }
</style>
@endsection