@extends('layouts.frontend')

@section('content')
<section>
    <div class="container">
        <div class="heading heading-center">
            <h2>HUBUNGI KAMI</h2>
        </div>

        <div class="row team-members m-b-40">
            @forelse ($models as $contact)
            <div class="col-md-3">
                <div class="team-member">
                    <div>
                        <img width="200" height="200" src="{{ asset('storage/'.$contact->avatar) }}">
                    </div>

                    <br>
                    <div class="team-desc">
                        <h3>{{ $contact->name }}</h3>
                        <span>{{ $contact->position }}</span><br>
                        <!--<p>{{ str_limit($contact->description,30) }}</p>-->

                        <div class="align-center">
                            <a href="https://wa.me/{{ $contact->contacts }}?text={{ urlencode("Haloo ". $contact->name) }}">
                                <img width="145" height="50"
                                    src="https://tribulant.com/blog/wp-content/uploads/2018/08/whatsapp-button.png"></img>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <div class="text-center">
                <h3>Belum ada kontak</h3>
            </div>
            @endforelse
        </div>
    </div>
</section>
@endsection
