@extends('layouts.frontend')

@section('title', 'Kegiatan '.$gallery->name .' |')

@section('content')
<div class="container">
    <div class="row">
        <!-- content -->
        <div class="content col-md-12">
            <!-- Blog -->
            <div id="blog" class="single-post">
                <!-- Post single item-->
                <div class="post-item">
                    <div class="post-item-wrap">
                        <div class="post-image">
                            <img alt="{{ $gallery->name }}" src="{{ $gallery->showImage() }}">
                        </div>
                        <div class="post-item-description">
                            <h2>{{$gallery->name}}</h2>
                            <div class="post-meta">
                                <span class="post-meta-date"><i class="fa fa-calendar-o"></i>{{ $gallery->created_at->format('D, d-m-Y') }}</span>
                            </div>
                            <p>{!!$gallery->description!!}</p>
                            <div class="container">
                                <div class="grid-layout grid-3-columns" data-margin="20" data-item="grid-item" data-lightbox="gallery">
                                @forelse ($galleryImage as $img)
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="{{$img->show()}}" data-lightbox="gallery-item"><img
                                                src="{{$img->show()}}"></a>
                                    </div>
                                @empty
                                </div>
                                <h1 class="text-center">Tidak ada dokumentasi untuk {{$gallery->name}}</h1>
                                @endforelse
                            </div>
                        </div>

                        <div class="post-navigation">
                            @if ($postBefore !== 'none')
                            <a href="{{route('frontend.activities.detail',['slug'=>$postBefore->slug])}}" class="post-prev">
                                <div class="post-prev-title"><span>Previous Post</span>{{$postBefore->name}}
                                </div>
                            </a>
                            @endif
                            <a href="{{route('frontend.activities')}}" class="post-all">
                                <i class="fa fa-th"></i>
                            </a>
                            @if ($postAfter !== 'none')
                            <a href="{{route('frontend.activities.detail',['slug'=>$postAfter->slug])}}" class="post-next">
                                <div class="post-next-title"><span>Next Post</span>{{$postAfter->name}}</div>
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- end: Post single item-->
            </div>
        </div>
        <!-- end: content -->
    </div>
</div>
@endsection
