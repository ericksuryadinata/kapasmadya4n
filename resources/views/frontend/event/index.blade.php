@extends('layouts.frontend')

@section('title', 'Daftar kegiatan warga kapas madya 4N |')

@section('content')
<div class="container">
    <div class="page-title text-center">
        <h1 class="p-b-10">Kegiatan Warga</h1>
    </div>
    <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
        @forelse ($galleries as $gallery)
        <div class="post-item border">
            <div class="post-item-wrap">
                <div class="post-image">
                    <a href="{{route('frontend.activities.detail',["slug" => $gallery->slug])}}">
                        <img alt="{{ $gallery->name }}" src="{{ $gallery->showImage() }}">
                    </a>
                </div>
                <div class="post-item-description">
                    <span class="post-meta-date"><i class="fa fa-calendar-o"></i>{{ $gallery->created_at->format('D, d-m-Y') }}</span>
                    <h2><a href="{{route('frontend.activities.detail',["slug" => $gallery->slug])}}">{{ $gallery->name }}</a>
                    </h2>
                </div>
            </div>
        </div>
        @empty
        <div class="text-center">
            <h3>Belum ada kegiatan warga</h3>
        </div>
        @endforelse
    </div>
</div>

@endsection
