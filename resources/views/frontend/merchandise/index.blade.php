@extends('layouts.frontend')

@section('title', 'Produk & kerajinan warga |')

@section('content')
<div class="container">
    <div class="page-title text-center">
        <h1>Produk Warga</h1>
    </div>
    <div class="row">
        <div class="well">
            <div class="list-group">
                @forelse ($merchandises as $merchandise)
                <div class="list-group-item">
                    <div class="media col-md-3">
                        <figure class="pull-left">
                            <img class="media-object img-rounded img-responsive" src="{{ $merchandise->showImage() }}"
                                alt="{{ $merchandise->name }}">
                        </figure>
                    </div>
                    <div class="col-md-6">
                        <h4 class="list-group-item-heading"> {{ ucwords($merchandise->name) }} </h4>
                        <p class="list-group-item-text">{{ $merchandise->description }}</p>
                    </div>
                    <div class="col-md-3 text-center">
                        <h2>{{ $merchandise->Harga() }}</h2>
                        <a href="https://wa.me/{{ $merchandise->Whatsapp() }}?text={{ urlencode("Saya mau memesan". $merchandise->name) }}">
                            <img width="145" height="50"
                                src="https://tribulant.com/blog/wp-content/uploads/2018/08/whatsapp-button.png"></img>
                        </a>
                    </div>
                </div>
                @empty
                <div class="text-center">
                    <h1>Tidak ada item</h1>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style type="text/css">
	div.list-group-item {
	    height:auto;
	    min-height:220px;
	    overflow: hidden;
        margin-bottom: 20px;
	}
	div.list-group-item:hover{
	    background-color: #f2f9fa;
	}
	a.list-group-item.active small {
	    color:#fff;
	}
	.stars {
	    margin:20px auto 1px;
	}
</style>
@endsection
