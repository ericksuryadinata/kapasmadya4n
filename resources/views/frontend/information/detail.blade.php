@extends('layouts.frontend')

@section('title', 'Informasi mengenai '.$article->name.' |')

@section('content')
            <div class="container">
                <div class="row">
                    <!-- content -->
                    <div class="content col-md-9">
                        <!-- Blog -->
                        <div id="blog" class="single-post">
                            <!-- Post single item-->
                            <div class="post-item">
                                <div class="post-item-wrap">
                                    <div class="carousel dots-inside arrows-visible" data-items="1" data-lightbox="gallery">
                                    	@foreach ($articleImage as $image)
                                        <a href="#" data-lightbox="gallery-item">
                                            <img alt="image" src="{{ $image->showImage() }}">
                                        </a>
                                    	@endforeach
                                    </div>
                                    <div class="post-item-description">
                                        <h2>{{ $article->name }}</h2>
                                        <div class="post-meta">

                                            <span class="post-meta-date"><i class="fa fa-calendar-o"></i>{{ $article->created_at }}</span>
                                            <div class="post-meta-share">
                                                <a class="btn btn-xs btn-slide btn-facebook" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                    <span>Facebook</span>
                                                </a>
                                                <a class="btn btn-xs btn-slide btn-twitter" href="#" data-width="100">
                                                    <i class="fa fa-twitter"></i>
                                                    <span>Twitter</span>
                                                </a>
                                                <a class="btn btn-xs btn-slide btn-instagram" href="#" data-width="118">
                                                    <i class="fa fa-instagram"></i>
                                                    <span>Instagram</span>
                                                </a>
                                                <a class="btn btn-xs btn-slide btn-googleplus" href="mailto:#" data-width="80">
                                                    <i class="fa fa-envelope"></i>
                                                    <span>Mail</span>
                                                </a>
                                            </div>
                                        </div>
                                        {!! $article->description !!}
                                    </div>

                                    <div class="post-navigation">
                                        <a href="{{ route('frontend.informasi.detail', ($postBefore != 'none') ? $postBefore->slug : '#') }}" class="post-prev">
                                            <div class="post-prev-title"><span>Informasi sebelumnya</span>{{ ($postBefore != 'none') ? $postBefore->name : 'Belum ada informasi sebelumnya' }}</div>
                                        </a>
                                        <a href="{{ route('frontend.informasi') }}" class="post-all">
                                            <i class="fa fa-th"></i>
                                        </a>

                                        <a href="{{ route('frontend.informasi.detail', ($postBefore != 'none') ? $postBefore->slug : '#') }}" class="post-next">
                                            <div class="post-next-title"><span>Informasi berikutnya</span>
                                            	{{ ($postAfter != 'none') ? $postAfter->name : 'Belum ada informasi berikutnya' }}
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- end: Post single item-->
                        </div>
                    </div>
                </div>
            </div>
@endsection