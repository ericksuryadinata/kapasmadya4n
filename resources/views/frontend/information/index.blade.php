@extends('layouts.frontend')

@section('title', 'Daftar informasi |')

@section('content')
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9">
                <!-- Page title -->
                <div class="page-title">
                    <h1>Informasi</h1>
                    <div class="breadcrumb float-left">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li class="active"><a href="#">Informasi</a></li>
                        </ul>
                    </div>
                </div>
                <!-- end: Page title -->

                <!-- Blog -->
                <div id="blog" class="post-thumbnails">

                    <!-- Post item-->
                    @if (count($models) < 0)
                    <h1>Belum ada informasi</h1>
                    @else
                    @foreach ($models as $model)

                    <div class="post-item">
                        <div class="post-item-wrap">
                            <div class="post-image">
                                <a href="{{ route('frontend.informasi.detail', $model->id) }}">
                                    <img alt="" src="{{ $model->showBanner() }}">
                                </a>
                            </div>
                            <div class="post-item-description">
                                <h2><a href="{{ route('frontend.informasi.detail', $model->id) }}">{{ ucfirst($model->name) }}</a></h2>
                                {!! str_limit($model->description, 200) !!}

                                <a href="{{ route('frontend.informasi.detail', $model->id) }}" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>

                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    <!-- end: Post item-->
                </div>
                <!-- end: Blog -->
                
                <!-- Pagination -->
                <div class="pagination pagination-simple">
                    	{{ $models->links() }}
                    {{-- <ul> --}}
                        {{-- <li>
                            <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a>
                        </li>
                        <li><a href="#">1</a> </li>
                        <li><a href="#">2</a> </li>
                        <li class="active"><a href="#">3</a> </li>
                        <li><a href="#">4</a> </li>
                        <li><a href="#">5</a> </li>
                        <li>
                            <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a>
                        </li> --}}
                    {{-- </ul> --}}
                </div>
                <!-- end: Pagination -->

            </div>
            <!-- end: post content -->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <div class="pinOnScroll">
                    <!--Tabs with Posts-->
                    <div class="widget ">
                        <h4 class="widget-title">Informasi Terbaru</h4>
                        <div class="post-thumbnail-list">
                        	@foreach ($rescentPost as $rescent)
                            <div class="post-thumbnail-entry">
                                <img alt="" src="{{ asset('storage/'.$rescent->banner) }}">
                                <div class="post-thumbnail-content">
                                    <a href="#">{{ ucfirst($rescent->name) }}</a>
                                    {{-- <span class="post-date"><i class="fa fa-clock-o"></i> {{ date(, strtotime($rescent->created_at) }}) 6m ago</span> --}}
                                </div>
                            </div>
                        	@endforeach
                        </div>
                    </div>
                    <!--End: Tabs with Posts-->
                         
                    <!--end: widget newsletter-->
                </div>
            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
@endsection