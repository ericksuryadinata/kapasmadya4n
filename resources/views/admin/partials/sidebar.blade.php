<div class="sidebar sidebar-main">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user-material">
			<div class="category-content">
				<div class="sidebar-user-material-content">
					<a href="#"><img src="{{ asset('admin/images/avatar-1.png') }}" class="img-circle img-responsive" alt=""></a>
					<h6>{{ ucwords(Auth::user()->name) }}</h6>
					<span class="text-size-small">{{ ucwords(Auth::user()->permition) }}</span>
				</div>

				<div class="sidebar-user-material-menu">
					<a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
				</div>
			</div>

			<div class="navigation-wrapper collapse" id="user-nav">
				<ul class="navigation">
					<li><a href="#"><i class="icon-user-plus"></i> <span>My profile</span></a></li>
					<li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>
					<li><a href="#"><i class="icon-comment-discussion"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
					<li><a href="{{ route('admin.signout') }}"><i class="icon-switch2"></i> <span>Logout</span></a></li>
				</ul>
			</div>
		</div>
		<!-- /user menu -->


		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Main -->
					<li class="navigation-header"><span>Menu &nbsp;<i class="icon-menu" title="Main pages"></i></span></li>
					<li @if(Request::segment(2) == null) class="active" @endif><a href="{{route('admin.index')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
					<li @if (Request::segment(2) == 'merchandise') class="active" @endif>
						<a href="{{ route('admin.merchandise.index') }}"><i class="icon-basket"></i> <span>Produk Warga</span></a>
                    </li>
					<li @if (Request::segment(2) == 'gallery') class="active" @endif>
						<a href="{{ route('admin.gallery.index') }}"><i class="icon-shutter"></i> <span>Kegiatan Warga</span></a>
                    </li>
                    <li @if (Request::segment(2) == 'news' ) class="active" @endif>
                        <a href="{{ route('admin.news.index') }}"><i class="icon-newspaper"></i> <span>Berita</span></a>
                    </li>
                    <li @if (Request::segment(2) == 'informasi' ) class="active" @endif>
                    	<a href="{{ route('admin.article.index') }}"><i class="icon-search4"></i> Informasi</a>
                    </li>
					<li @if (Request::segment(2) == 'contactus') class="active" @endif>
						<a href="{{ route('admin.contactus.index') }}"><i class="icon-address-book"></i> <span>Kontak</span></a>
					</li>
					<li @if(Request::segment(2) == 'slider') class="active" @endif>
						<a href="{{ route('admin.slider.index') }}"><i class="icon-image-compare"></i> <span>Slider</span></a>
					</li>
					<li @if(Request::segment(2) == 'config') class="active" @elseif(Request::segment(2) == 'seo') class="active" @elseif(Request::segment(2) == 'contact') class="active" @elseif(Request::segment(2) == 'user') class="active" @elseif(Request::segment(2) == 'logo') class="active" @endif><a href="{{route('admin.config.edit', 1)}}"><i class="icon-cog3"></i> <span>Pengaturan</span></a></li>
				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>
