<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{asset('admin/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('admin/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('admin/css/core.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('admin/css/components.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('admin/css/colors.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('js/sweetalert2.min.css') }}">
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.js"></script> --}}
  {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.css" id="theme-styles"> --}}
    <!-- /global stylesheets -->
    @yield('css')
