@extends('layouts.admin')

@section('title')
Semua data {{str_replace('-',' ',Request::segment(2))}}
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><span class="text-semibold">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</span> - Semua data {{str_replace('-',' ',Request::segment(2))}}</h4>
		</div>

		<div class="heading-elements">
			{{-- <div class="heading-btn-group"> --}}
				<a href="{{route('admin.article.create')}}" class="btn bg-teal-400 btn-labeled"><b><i class="icon-googleplus5"></i></b>Tambahkan {{str_replace('-',' ',Request::segment(2))}}</a>
			{{-- </div> --}}
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
	<div class="panel panel-flat">

		<table class="table datatable-basic {{-- table-bordered --}} article-table">
			<thead>
				<tr class="bg-slate">
					<th>No</th>
					<th>Image Preview</th>
					<th>Deskripsi</th>
					<th>Status</th>
					<th class="text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
				{{-- @dump($models) --}}
				@foreach($models as $key => $model)
				<tr>
					<td>{{ $key+1 }}</td>
					<td width="200">
						<img class="img-thumbnail" src="{{$model->showBanner()}}" alt="" style="width: 100%">
					</td>
					<td>
						<span>{!! str_limit($model->description, '150') !!}</span>
					</td>
					<td>
						<label class="label {{ ($model->status == 1) ? 'bg-primary' : 'bg-danger' }}">{{ ($model->status == 1) ? 'Active' : 'Non Active' }}</label>
					</td>
					<td class="text-center">
						<ul class="icons-list">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-menu9"></i>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="{{ route('admin.article.edit', $model->id) }}"><i class="icon-pencil"></i> Edit</a></li>
									<li><a class="delete-modal" href="{{route('admin.article.destroy', $model->id)}}"><i class="icon-trash"></i> Hapus</a></li>
								</ul>
							</li>
						</ul>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
    </div>
    <form id="form-delete" method="post" hidden>
        {{csrf_field()}} {{method_field('DELETE')}}
    </form>
@endsection

@section('min-js')
	<script type="text/javascript" src="{{ asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
@endsection
@section('js')
    <script type="text/javascript" src="{{ asset('admin/js/pages/datatables_basic.js')}}"></script>
    <script type="text/javascript">
        $(".article-table").on('click', '.delete-modal', function (e) {
            e.preventDefault();
            let href = $(this).attr('href');
            $('#form-delete').attr('action', href);
            $('#form-delete').submit();
        });
    </script>
@endsection
