@extends('layouts.admin')

@section('title')
Perbarui {{ucwords(str_replace('-',' ',Request::segment(2)))}}
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><span class="text-semibold">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</span> - Perbarui
                {{ucwords(str_replace('-',' ',Request::segment(2)))}} </h4>
		</div>

		<div class="heading-elements">
			{{-- <div class="heading-btn-group"> --}}
				<a href="{{route('admin.article.index')}}" class="btn bg-teal-400 btn-labeled"><b><i class="icon-square-left"></i></b>Kembali</a>
			{{-- </div> --}}
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{ route('admin.article.index') }}">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</a></li>
			<li class="active">Perbarui</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<form action="{{ route('admin.article.update', $model->id) }}" method="post" enctype="multipart/form-data" id="formdata">
	@csrf
	@method('PUT')
	<div class="col-lg-12">
		@if ($errors->any())
		@foreach ($errors->all() as $error)
		<label class="label bg-danger">{{ $error }}</label>
		@endforeach
		@endif
		<div class="panel panel-flat panel-body">
			<div class="form-group">
				<label>Nama</label>
				<input type="name" name="name" class="form-control" placeholder="Nama informasi" value="{{ $model->name }}">
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Gambar <b class="text-danger">*</b></label>
						<input type="file" name="banner" class="file-input">
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
						<label class="control-label">Status <b class="text-danger">*</b></label>
						<div class="checkbox checkbox-switch">
							<label>
								<input type="checkbox" name="status" class="switch" {{ $model->status == 1 ? 'checked' : '' }}>
							</label>
						</div>
					</div>
					<div class="form-group">
						<label>Detail Gambar informasi</label>
						<div class="dropzone" id="imageUpload">
							
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Deskripsi</label>
				<textarea id="editor-full" name="description">{{ $model->description }}</textarea>
			</div>
		</div>
		<button class="btn btn-primary">Simpan</button>
	</div>
</form>
@endsection

@section('min-js')
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/validation/validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/uploaders/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/uploaders/dropzone.min.js') }}"></script>
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function(){
		$('#formdata').submit(function(e) {
			e.preventDefault();
			$.ajax({
				url: $(this).attr('action'),
				data: new FormData(this),
				method: 'post',
	            contentType: false,
	            processData: false,
				beforeSend:function(){
					$('body').block({
						message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Data sedang di proses ... </span>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: '10px 15px',
							color: '#fff',
							width: 'auto',
							'-webkit-border-radius': 5,
							'-moz-border-radius': 5,
							backgroundColor: '#333'
						}
					});
				}
			}).done(function(e) {
				$('body').unblock();
				var json = $.parseJSON(e);
				// console.log(json.response.url);
		        new PNotify({
		            title: json.response.status,
		            text: json.response.msg,
		            addclass: 'bg-'+json.response.status
		        });
				if (json.status) {
	    			setTimeout(function(){
	    				window.location.href = json.response.url;
	    			},2500);
				} else {
					return;
				}
			}).fail(function(e) {
				$('body').unblock();
				console.log(e);
			})
		});

        $("#description").on('input propertychange', function() {
		    if (this.value.length >= 201) {
                $("#notify").fadeIn();
            } else {
                $("#notify").fadeOut();
                $("#counter").html(this.value.length);
            }
        });
	});

	$(".switch").bootstrapSwitch();
    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });
    // File input
    $(".file-styled").uniform({
        wrapperClass: 'bg-blue',
        fileButtonHtml: '<i class="icon-file-plus"></i>'
    });

	$('.select2').select2({
		placeholder: "- Pilih Kategori -",
		minimumResultsForSearch: Infinity
	});

	$(function() {

	    CKEDITOR.replace( 'editor-full', {
	        height: '262px',
	    });
	    
	    CKEDITOR.disableAutoInline = true;

	});

	$(".file-styled").uniform({
	    fileButtonClass: 'action btn bg-pink-400'
	});

	$(".mini-upload").change(function(){
	    var input       = this;
	    var image       = $(this).parents('.media-parent').find('img');
	    var lightbox    = $(this).parents('.media-parent').find('[data-popup="lightbox"]');
	    
	     if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $(lightbox).attr('href', e.target.result);
	            $(image).attr('src', e.target.result);

	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	});

	Dropzone.options.imageUpload = {
		headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
		paramName: 'file',
        dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
		url: "{{ route('admin.article.store_image') }}",
		maxFilesize: 12,
		acceptedFiles: ".jpeg,.jpg,.png,.gif",
		addRemoveLinks: true,
		timeout: 5000,
        maxFilesize: 2,
		parallelUploads:1,
		init:function(){
			// Add server images
			var myDropzone = this;
			var articleId = '{{ $model->id }}'
			$.ajax({
				headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
				type: 'GET',
				url: '{{ route('admin.article.get_image') }}',
				data: {id: articleId}
			}).done(function(data){
				console.log(data);
				let public_path = '{{ url("storage") }}';
				$.each(data.images, function (key, value) {
					var file = {name: value.original, size: value.size};
					myDropzone.options.addedfile.call(myDropzone, file);
					myDropzone.options.thumbnail.call(myDropzone, file, public_path+"/"+value.original);
					let fileuploaded = file.previewElement.querySelector("[data-dz-name]");
					// append the response key to create data-key
					fileuploaded.dataset.key = value.key;
					fileuploaded.innerHTML = value.name;
					myDropzone.emit("complete", file);
				});
			}).error(function(data){
				Swal.fire('Error', 'error getting data, please refresh the page','error');
			});

			this.on("removedfile", function(file, response){
				let dz_name = file.previewElement.querySelector("[data-dz-name]");
				// take the data-key
				let key = dz_name.dataset.key;
				$.ajax({
					headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
					type: 'POST',
					url: '{{ route('admin.article.delete_image') }}',
					data: { filekey: key },
					success: function (data){
                        new PNotify({
                            title: 'success',
                            text: 'File has been successfully removed!!',
                            addclass: 'bg-success'
                        });
					},
					error: function(e) {
                        console.log(e);
                        new PNotify({
                            title: 'error',
                            text: 'File not removed',
                            addclass: 'bg-danger'
                        });
					}
				});
				let fileRef;
				return (fileRef = file.previewElement) != null ?
				fileRef.parentNode.removeChild(file.previewElement) : void 0;
			});
		},
		success: function(file, response)
		{
			let fileuploaded = file.previewElement.querySelector("[data-dz-name]");
			// append the response key to create data-key
			fileuploaded.dataset.key = response.key;
			fileuploaded.innerHTML = response.name;
            new PNotify({
                title: 'success',
                text: 'Upload Success',
                addclass: 'bg-success'
            });
		},
		error: function(file, response)
		{
            let fileRef;
            return (fileRef = file.previewElement) != null ?
            fileRef.parentNode.removeChild(file.previewElement) : void 0;
            new PNotify({
                title: 'error',
                text: 'File not uploaded',
                addclass: 'bg-danger'
            });
		}
    };

	var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
	    '  <div class="modal-content">\n' +
	    '    <div class="modal-header">\n' +
	    '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
	    '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
	    '    </div>\n' +
	    '    <div class="modal-body">\n' +
	    '      <div class="floating-buttons btn-group"></div>\n' +
	    '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
	    '    </div>\n' +
	    '  </div>\n' +
	    '</div>\n';

	// Buttons inside zoom modal
	var previewZoomButtonClasses = {
	    toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
	    fullscreen: 'btn btn-default btn-icon btn-xs',
	    borderless: 'btn btn-default btn-icon btn-xs',
	    close: 'btn btn-default btn-icon btn-xs'
	};

	// Icons inside zoom modal classes
	var previewZoomButtonIcons = {
	    prev: '<i class="icon-arrow-left32"></i>',
	    next: '<i class="icon-arrow-right32"></i>',
	    toggleheader: '<i class="icon-menu-open"></i>',
	    fullscreen: '<i class="icon-screen-full"></i>',
	    borderless: '<i class="icon-alignment-unalign"></i>',
	    close: '<i class="icon-cross3"></i>'
	};

	// File actions
	var fileActionSettings = {
	    zoomClass: 'btn btn-link btn-xs btn-icon',
	    zoomIcon: '<i class="icon-zoomin3"></i>',
	    dragClass: 'btn btn-link btn-xs btn-icon',
	    dragIcon: '<i class="icon-three-bars"></i>',
	    removeClass: 'btn btn-link btn-icon btn-xs',
	    removeIcon: '<i class="icon-trash"></i>',
	    indicatorNew: '<i class="icon-file-plus text-slate"></i>',
	    indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
	    indicatorError: '<i class="icon-cross2 text-danger"></i>',
	    indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
	};

	var imagePreview 	= ["<img class='col-lg-6' src='{{ asset($model->showBanner()) }}'>"];

	//
	// Basic example
	//

	$('.file-input').fileinput({
	    browseLabel: 'Browse',
	    browseIcon: '<i class="icon-file-plus"></i>',
	    uploadIcon: '<i class="icon-file-upload2"></i>',
	    removeIcon: '<i class="icon-cross3"></i>',
	    layoutTemplates: {
	        icon: '<i class="icon-file-check"></i>',
	        modal: modalTemplate
	    },
	    maxFileCount: 4,
	    maxFileSize: 3000,
	    allowedFileExtensions: ["jpg", "png","jpeg"],
	    initialCaption: "Tidak ada file yang dipilih",
	    initialPreview: imagePreview,
	    previewZoomButtonClasses: previewZoomButtonClasses,
	    previewZoomButtonIcons: previewZoomButtonIcons,
	    fileActionSettings: fileActionSettings
	});
</script>
@endsection

@section('css')
<style>
    .dz-image img{
        height: 150px;
    }
</style>
@endsection
