
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login Administrator</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/core.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/components.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('admin/css/colors.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('admin/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/loaders/blockui.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/notifications/pnotify.min.js') }}"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
	{{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script> --}}
	<!-- /core JS files -->


	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('admin/js/core/app.js') }}"></script>

	<script type="text/javascript" src="{{ asset('admin/js/plugins/ui/ripple.min.js') }}"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">
<br><br><br>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content" >

					<!-- Simple login form -->
					<form id="authentication" action="{{ route('auth.admin') }}" method="post">
						@csrf
						<div class="panel panel-body login-form">
							@if($errors->any())
								@foreach($errors->all() as $error)
									<div class="alert bg-danger alert-styled-left">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
										<span class="text-semibold">{{ $error }}</span>
								    </div>
								@endforeach
							@endif
							<div class="text-center">
								<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
								<h5 class="content-group">Sistem Administrator <small class="display-block">Masukkan username dan password</small></h5>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" placeholder="Password" name="password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn bg-pink-400 btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
							</div>
						</div>
					</form>
					<!-- /simple login form -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#authentication').submit(function(e) {
			e.preventDefault();
			$.ajax({
				url: $(this).attr('action'),
				data: $(this).serialize(),
				method: 'post',
				beforeSend:function(){
					$('body').block({
						message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Authentikasi </span>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: '10px 15px',
							color: '#fff',
							width: 'auto',
							'-webkit-border-radius': 5,
							'-moz-border-radius': 5,
							backgroundColor: '#333'
						}
					});
				}
			}).done(function(e) {
				$('body').unblock();
				var json = $.parseJSON(e);
				// console.log(json.response.url);
		        new PNotify({
		            title: json.response.status,
		            text: json.response.msg,
		            addclass: 'bg-'+json.response.bg
		        });
				if (json.status) {
	    			setTimeout(function(){
	    				window.location.href = json.response.url;
	    			},2500);
				} else {
					return;
				}
			}).fail(function(e) {
				$('body').unblock();
				console.log(e);
			})
		})
	});
</script>
</body>
</html>
