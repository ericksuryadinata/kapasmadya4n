@extends('layouts.admin')

@section('title')
Perbarui merchandise {{ $model->name }}
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Kontak Panitia</span> - Perbarui kontak panitia baru</h4>
		</div>

		<div class="heading-elements">
			<a href="{{route('admin.contactus.index')}}" class="btn bg-teal-400 btn-labeled"><b><i class="icon-square-left"></i></b>Kembali</a>
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{ route('admin.contactus.index') }}">Kontak Panitia</a></li>
			<li class="active">PErbarui</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<form id="formdata" action="{{route('admin.contactus.update', $model->id)}}" method="post" enctype="multipart/form-data">
	@csrf
	@method('PUT')
	<div class="row">
		<div class="col-lg-12">
			@if($errors->any())
				@foreach($errors->all() as $error)
					<div class="alert bg-danger alert-styled-left">
						<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
						<span class="text-semibold">{{ $error }}</span>
				    </div>
				@endforeach
			@endif
			<div class="panel panel-flat">
				<div class="panel-body">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Name <b class="text-danger">*</b></label>
							<input type="text" name="name" placeholder="ex: John Doe" class="form-control" value="{{ $model->name }}">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Posisi <b class="text-danger">*</b></label>
							<input type="text" name="position" class="form-control" placeholder="ex: Manager" value="{{ $model->position }}">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label>Avatar <b class="text-danger">*</b></label>
							<input name="avatar" type="file" class="file-input" 	>
							<div class="help-block">
								- Ukuran Gambar Maks. 3MB<br>
								- Format yang diperbolehkan (jpg, png, jpeg).	
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Kontak Whatsapp <b class="text-danger">*</b></label>
							<input type="text" name="whatsapp" placeholder="ex: 08324232362" class="form-control" value="{{ $model->whatsapp }}">
						</div>
						<div class="form-group">
							<label>Deskripsi</label>
							<textarea class="form-control" name="description" id="description" rows="6" placeholder="Deskripsi barang merchandise max 200 karakter" maxlength="201">{{ $model->description }}</textarea>
							<span id="notify" class="text-danger" style="display: none">Batas karakter sudah mencapai 200</span>
							<span id='counter' class="pull-right text-justify"></span>
						</div>
					</div>
					<div class="col-lg-12">
					</div>
				</div>
			</div>
			<button class="btn btn-primary pull-right">Tambahkan Kontak <i class="icon-arrow-right14 position-right"></i></button>
		</div>
	</div>
</form>
@endsection

@section('min-js')
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/validation/validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/uploaders/fileinput.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/notifications/pnotify.min.js') }}"></script>
@endsection

@section('js')
	<script type="text/javascript">
		$(document).ready(function(){
			$('#formdata').submit(function(e) {
				e.preventDefault();
				$.ajax({
					url: $(this).attr('action'),
					data: new FormData(this),
					method: 'post',
		            contentType: false,
		            processData: false,
					beforeSend:function(){
						$('body').block({
							message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Data sedang di proses ... </span>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: '10px 15px',
								color: '#fff',
								width: 'auto',
								'-webkit-border-radius': 5,
								'-moz-border-radius': 5,
								backgroundColor: '#333'
							}
						});
					}
				}).done(function(e) {
					$('body').unblock();
					var json = $.parseJSON(e);
					// console.log(json.response.url);
					if (json.status) {
			        new PNotify({
			            title: json.response.status,
			            text: json.response.msg,
			            addclass: 'bg-'+json.response.status
			        });
		    			setTimeout(function(){
		    				window.location.href = json.response.url;
		    			},2000);
					} else {
						return;
					}
				}).fail(function(e) {
					$('body').unblock();
					console.log(e);
				})
			});
		});

		$("#description").on('input propertychange', function() {
			if (this.value.length >= 201) {
				$("#notify").fadeIn();				
			} else {
				$("#notify").fadeOut();
				$("#counter").html(this.value.length);
			}
		});


    // Modal template
    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header">\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n';

    // Buttons inside zoom modal
    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
        fullscreen: 'btn btn-default btn-icon btn-xs',
        borderless: 'btn btn-default btn-icon btn-xs',
        close: 'btn btn-default btn-icon btn-xs'
    };

    // Icons inside zoom modal classes
    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross3"></i>'
    };

    // File actions
    var fileActionSettings = {
        zoomClass: 'btn btn-link btn-xs btn-icon',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        dragClass: 'btn btn-link btn-xs btn-icon',
        dragIcon: '<i class="icon-three-bars"></i>',
        removeClass: 'btn btn-link btn-icon btn-xs',
        removeIcon: '<i class="icon-trash"></i>',
        indicatorNew: '<i class="icon-file-plus text-slate"></i>',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    };


    //
    // Basic example
    //

    $('.file-input').fileinput({
        browseLabel: 'Browse',
        browseIcon: '<i class="icon-file-plus"></i>',
        uploadIcon: '<i class="icon-file-upload2"></i>',
        removeIcon: '<i class="icon-cross3"></i>',
        layoutTemplates: {
            icon: '<i class="icon-file-check"></i>',
            modal: modalTemplate
        },
        initialPreview: [
            "{{ $model->showImage() }}",
        ],
        initialPreviewAsData: true,
        overwriteInitial: false,
        maxFileSize: 100,
        previewZoomButtonClasses: previewZoomButtonClasses,
        previewZoomButtonIcons: previewZoomButtonIcons,
        fileActionSettings: fileActionSettings
    });

	</script>
	{{-- <script type="text/javascript" src="{{ asset('admin/js/pages/uploader_bootstrap.js') }}"></script> --}}
	<script type="text/javascript" src="{{asset('admin/js/pages/form_validation.js')}}"></script>
@endsection