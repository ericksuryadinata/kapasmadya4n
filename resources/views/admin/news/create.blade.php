@extends('layouts.admin')

@section('title')
Buat berita baru
@endsection

@section('heading')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</span> - Buat
                {{ucwords(str_replace('-',' ',Request::segment(2)))}} baru</h4>
        </div>

        <div class="heading-elements">
            <a href="{{route('admin.news.index')}}" class="btn bg-teal-400 btn-labeled"><b><i
                        class="icon-square-left"></i></b>Kembali</a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('admin.news.index') }}">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</a></li>
            <li class="active">Buat</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="formdata" action="{{route('admin.news.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-lg-12">
            @if($errors->any())
            @foreach($errors->all() as $error)
            <div class="alert bg-danger alert-styled-left">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span
                        class="sr-only">Close</span></button>
                <span class="text-semibold">{{ $error }}</span>
            </div>
            @endforeach
            @endif
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Judul Berita <b class="text-danger">*</b></label>
                            <input type="text" name="name" class="form-control" placeholder="ex: Tasbih kayu ulin">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Banner Berita </label>
                            <input name="image" type="file" class="file-input" data-show-upload='false'>
                            <div class="help-block">
                                - Ukuran Gambar Maks. 3MB<br>
                                - Format yang diperbolehkan (jpg, png, jpeg).
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <div class="checkbox checkbox-switch">
                                <input name="status" type="checkbox" data-on-text="Aktif" data-off-text="Non-aktif" class="switch">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Isi Berita</label>
                            <textarea cols="18" rows="18" class="wysihtml5 wysihtml5-min form-control" name="description" id="description"
                                placeholder="Isi Berita"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary pull-right">Tambahkan Berita <i
                    class="icon-arrow-right14 position-right"></i></button>
        </div>
    </div>
</form>
@endsection

@section('min-js')
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/validation/validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switch.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/editors/wysihtml5/wysihtml5.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/editors/wysihtml5/toolbar.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/editors/wysihtml5/parsers.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/uploaders/fileinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/notifications/pnotify.min.js') }}"></script>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('.wysihtml5-min').wysihtml5({
            parserRules: wysihtml5ParserRules,
            "font-styles": true, // Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, // Italics, bold, etc. Default true
            "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": false, // Button which allows you to edit the generated HTML. Default false
            "link": true, // Button to insert a link. Default true
            "image": false, // Button to insert an image. Default true,
            "action": false, // Undo / Redo buttons,
            "color": true // Button to change color of font
        });

        $('#formdata').submit(function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                data: new FormData(this),
                method: 'post',
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('body').block({
                        message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Data sedang di proses ... </span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 5,
                            '-moz-border-radius': 5,
                            backgroundColor: '#333'
                        }
                    });
                }
            }).done(function(e) {
                $('body').unblock();
                var json = $.parseJSON(e);
                // console.log(json.response.url);
                if (json.status) {
                    new PNotify({
                        title: json.response.status,
                        text: json.response.msg,
                        addclass: 'bg-'+json.response.status
                    });
                    setTimeout(function(){
                        window.location.href = json.response.url;
                    },2500);
                } else {
                    return;
                }
            }).fail(function(e) {
                $('body').unblock();
                console.log(e);
            });
        });
    });

</script>
<script type="text/javascript" src="{{ asset('admin/js/pages/uploader_bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/pages/form_validation.js') }}"></script>
@endsection
