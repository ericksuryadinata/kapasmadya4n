@extends('layouts.admin')

@section('title')
Buat berita baru
@endsection

@section('heading')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</span> - Buat
                {{ucwords(str_replace('-',' ',Request::segment(2)))}} baru</h4>
        </div>

        <div class="heading-elements">
            <a href="{{route('admin.news.index')}}" class="btn bg-teal-400 btn-labeled"><b><i
                        class="icon-square-left"></i></b>Kembali</a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('admin.news.index') }}">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</a></li>
            <li class="active">Buat</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="formdata" action="{{ route('admin.news.update', $news->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-lg-12">
            @if($errors->any())
            @foreach($errors->all() as $error)
            <div class="alert bg-danger alert-styled-left">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span
                        class="sr-only">Close</span></button>
                <span class="text-semibold">{{ $error }}</span>
            </div>
            @endforeach
            @endif
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Judul Berita <b class="text-danger">*</b></label>
                            <input type="text" name="name" class="form-control" placeholder="ex: Tasbih kayu ulin" value={{$news->name}}>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Banner Berita </label>
                            <input name="image" type="file" class="file-input" data-show-upload='false'>
                            <div class="help-block">
                                - Ukuran Gambar Maks. 3MB<br>
                                - Format yang diperbolehkan (jpg, png, jpeg).
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <div class="checkbox checkbox-switch">
                                <input name="status" type="checkbox" data-on-text="Aktif" data-off-text="Non-aktif" class="switch"
                                    {{ $news->status == 1 ? 'checked' : '' }}>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Isi Berita</label>
                            <textarea cols="18" rows="18" class="wysihtml5 wysihtml5-min form-control"
                                name="description" id="description" placeholder="Isi Berita">{{ $news->description}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary pull-right">Update Berita <i
                    class="icon-arrow-right14 position-right"></i></button>
        </div>
    </div>
</form>
@endsection

@section('min-js')
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/validation/validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switch.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/editors/wysihtml5/wysihtml5.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/editors/wysihtml5/toolbar.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/editors/wysihtml5/parsers.js')}}"></script>
<script type="text/javascript"
    src="{{ asset('admin/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/uploaders/fileinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/plugins/notifications/pnotify.min.js') }}"></script>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('.wysihtml5-min').wysihtml5({
            parserRules: wysihtml5ParserRules,
            "font-styles": true, // Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, // Italics, bold, etc. Default true
            "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": false, // Button which allows you to edit the generated HTML. Default false
            "link": true, // Button to insert a link. Default true
            "image": false, // Button to insert an image. Default true,
            "action": false, // Undo / Redo buttons,
            "color": true // Button to change color of font
        });

        $('#formdata').submit(function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                data: new FormData(this),
                method: 'post',
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $('body').block({
                        message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Data sedang di proses ... </span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 5,
                            '-moz-border-radius': 5,
                            backgroundColor: '#333'
                        }
                    });
                }
            }).done(function(e) {
                $('body').unblock();
                var json = $.parseJSON(e);
                // console.log(json.response.url);
                if (json.status) {
                    new PNotify({
                        title: json.response.status,
                        text: json.response.msg,
                        addclass: 'bg-'+json.response.status
                    });
                    setTimeout(function(){
                        window.location.href = json.response.url;
                    },2500);
                } else {
                    return;
                }
            }).fail(function(e) {
                $('body').unblock();
                console.log(e);
            });
        });
    });

    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header">\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n';

    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
        fullscreen: 'btn btn-default btn-icon btn-xs',
        borderless: 'btn btn-default btn-icon btn-xs',
        close: 'btn btn-default btn-icon btn-xs'
    };

    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross3"></i>'
    };

    var fileActionSettings = {
        zoomClass: 'btn btn-link btn-xs btn-icon',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        dragClass: 'btn btn-link btn-xs btn-icon',
        dragIcon: '<i class="icon-three-bars"></i>',
        removeClass: 'btn btn-link btn-icon btn-xs',
        removeIcon: '<i class="icon-trash"></i>',
        indicatorNew: '',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    };

    $('.file-input').fileinput({
        browseLabel: 'Browse',
        browseIcon: '<i class="icon-file-plus"></i>',
        layoutTemplates: {
            icon: '<i class="icon-file-check"></i>',
            modal: modalTemplate
        },
        initialPreview: [
            "{{ asset('storage/'.$news->image) }}",
        ],
        initialPreviewAsData: true,
        overwriteInitial: false,
        previewZoomButtonClasses: previewZoomButtonClasses,
        previewZoomButtonIcons: previewZoomButtonIcons,
        fileActionSettings: fileActionSettings
    });

</script>
<script type="text/javascript" src="{{ asset('admin/js/pages/form_validation.js') }}"></script>
@endsection
