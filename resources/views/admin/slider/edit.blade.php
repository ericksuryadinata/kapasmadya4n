@extends('layouts.admin')

@section('title')
Perbarui slider
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Slider</span> - Perbarui slider</h4>
		</div>

		<div class="heading-elements">
			<a href="{{route('admin.slider.index')}}" class="btn bg-teal-400 btn-labeled"><b><i class="icon-square-left"></i></b>Kembali</a>
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{ route('admin.slider.index') }}">Slider</a></li>
			<li class="active">Perbarui</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<form id="slider" action="{{route('admin.slider.update', $model->id)}}" method="post" enctype="multipart/form-data">
	@csrf
	@method('PUT')
	<div class="row">
		<div class="col-md-12">
			@if($errors->any())
				@foreach($errors->all() as $error)
					<div class="alert bg-danger alert-styled-left">
						<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
						<span class="text-semibold">{{ $error }}</span>
				    </div>
				@endforeach
			@endif
			<div class="panel panel-flat">
				<div class="panel-body">
					<div class="col-md-8">
						<div class="form-group">
							<label>Gambar Slider </label>
							<input name="image" type="file" class="file-input" data-show-upload="false" data-show-remove="false">
							<div class="help-block">
								- Ukuran Gambar Maks. 3MB<br>
								- Format yang diperbolehkan (jpg, png, jpeg).<br>
								- <span class="text-danger">Jangan di ubah jika tidak di perlukan</span>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Status</label>
							<div class="checkbox checkbox-switch">
								<input name="status" type="checkbox" data-on-text="Aktif" data-off-text="Non-aktif" class="switch" @if ($model->status == 1) checked @endif>
							</div>
						</div>
					</div>
				</div>
			</div>
			<button class="btn btn-primary pull-right">Perbarui Slider <i class="icon-arrow-right14 position-right"></i></button>
		</div>
	</div>
</form>
@endsection

@section('js')
{{-- <script type="text/javascript" src="{{ asset('admin/js/custome/lib.js') }}"></script> --}}
	<script type="text/javascript">
		$(document).ready(function(){
			$('#slider').submit(function(e) {
				e.preventDefault();
				$.ajax({
					url: $(this).attr('action'),
					data: new FormData(this),
					method: 'post',
		            contentType: false,
		            processData: false,
					beforeSend:function(){
						$('body').block({
							message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Data sedang di proses ... </span>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: '10px 15px',
								color: '#fff',
								width: 'auto',
								'-webkit-border-radius': 5,
								'-moz-border-radius': 5,
								backgroundColor: '#333'
							}
						});
					}
				}).done(function(e) {
					$('body').unblock();
					var json = $.parseJSON(e);
					// console.log(json.response.url);
				    new PNotify({
				        title: json.response.status,
				        text: json.response.msg,
				        addclass: 'bg-'+json.response.status
				    });
					if (json.status) {
		    			setTimeout(function(){
		    				window.location.href = json.response.url;
		    			},2000);
					} else {
						return;
					}
				}).fail(function(e) {
					$('body').unblock();
					console.log(e);
				})
			});

		    // Modal template
		    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
		        '  <div class="modal-content">\n' +
		        '    <div class="modal-header">\n' +
		        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
		        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
		        '    </div>\n' +
		        '    <div class="modal-body">\n' +
		        '      <div class="floating-buttons btn-group"></div>\n' +
		        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
		        '    </div>\n' +
		        '  </div>\n' +
		        '</div>\n';

		    // Buttons inside zoom modal
		    var previewZoomButtonClasses = {
		        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
		        fullscreen: 'btn btn-default btn-icon btn-xs',
		        borderless: 'btn btn-default btn-icon btn-xs',
		        close: 'btn btn-default btn-icon btn-xs'
		    };

		    // Icons inside zoom modal classes
		    var previewZoomButtonIcons = {
		        prev: '<i class="icon-arrow-left32"></i>',
		        next: '<i class="icon-arrow-right32"></i>',
		        toggleheader: '<i class="icon-menu-open"></i>',
		        fullscreen: '<i class="icon-screen-full"></i>',
		        borderless: '<i class="icon-alignment-unalign"></i>',
		        close: '<i class="icon-cross3"></i>'
		    };

		    // File actions
		    var fileActionSettings = {
		        zoomClass: 'btn btn-link btn-xs btn-icon',
		        zoomIcon: '<i class="icon-zoomin3"></i>',
		        dragClass: 'btn btn-link btn-xs btn-icon',
		        dragIcon: '<i class="icon-three-bars"></i>',
		        removeClass: 'btn btn-link btn-icon btn-xs',
		        removeIcon: '<i class="icon-trash"></i>',
		        indicatorNew: '<i class="icon-file-plus text-slate"></i>',
		        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
		        indicatorError: '<i class="icon-cross2 text-danger"></i>',
		        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
		    };

		    var imagePrev = "<img src='{{ $model->showImage() }}' class='file-preview-image' alt='image'>";
		    $(".file-input").fileinput({
		        browseLabel: 'Browse',
		        browseIcon: '<i class="icon-file-plus"></i>',
		        layoutTemplates: {
		            icon: '<i class="icon-file-check"></i>',
		            modal: modalTemplate
		        },

		        maxFileSize: 3000,
			    allowedFileExtensions: ["jpg", "png","jpeg"],
		        initialPreview: imagePrev,
		        previewZoomButtonClasses: previewZoomButtonClasses,
		        previewZoomButtonIcons: previewZoomButtonIcons,
		        fileActionSettings: fileActionSettings,
		    });
		});

	</script>
	{{-- <script type="text/javascript" src="{{ asset('admin/js/pages/uploader_bootstrap.js') }}"></script> --}}
	<script type="text/javascript" src="{{asset('admin/js/pages/form_validation.js')}}"></script>
@endsection
