@extends('layouts.admin')

@section('title')
Buat slider baru
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Slider</span> - Buat slider baru</h4>
		</div>

		<div class="heading-elements">
			{{-- <div class="heading-btn-group"> --}}
				<a href="{{route('admin.slider.index')}}" class="btn bg-teal-400 btn-labeled"><b><i class="icon-square-left"></i></b>Kembali</a>
			{{-- </div> --}}
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{ route('admin.slider.index') }}">Slider</a></li>
			<li class="active">Buat</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<form id="slider" action="{{route('admin.slider.store')}}" method="post" enctype="multipart/form-data">
	@csrf
	<div class="row">
		<div class="col-md-12">
			@if($errors->any())
				@foreach($errors->all() as $error)
					<div class="alert bg-danger alert-styled-left">
						<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
						<span class="text-semibold">{{ $error }}</span>
				    </div>
				@endforeach
			@endif
			<div class="panel panel-flat">
				<div class="panel-body">
					<div class="col-md-8">
						<div class="form-group">
							<label>Gambar Slider </label>
							<input name="image" type="file" class="file-input" data-show-upload="false">
							<div class="help-block">
								- Ukuran Gambar Maks. 3MB<br>
								- Format yang diperbolehkan (jpg, png, jpeg).
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Status</label>
							<div class="checkbox checkbox-switch">
								<input name="status" type="checkbox" data-on-text="Aktif" data-off-text="Non-aktif" class="switch">
							</div>
						</div>
					</div>
				</div>
			</div>
			<button class="btn btn-primary pull-right">Buat Slider <i class="icon-arrow-right14 position-right"></i></button>
		</div>
	</div>
</form>
@endsection

@section('min-js')
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/validation/validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/uploaders/fileinput.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/notifications/pnotify.min.js') }}"></script>
@endsection

@section('js')
	<script type="text/javascript">
		$(document).ready(function(){
			$('#slider').submit(function(e) {
				e.preventDefault();
				$.ajax({
					url: $(this).attr('action'),
					data: new FormData(this),
					method: 'post',
		            contentType: false,
		            processData: false,
					beforeSend:function(){
						$('body').block({
							message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Data sedang di proses ... </span>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: '10px 15px',
								color: '#fff',
								width: 'auto',
								'-webkit-border-radius': 5,
								'-moz-border-radius': 5,
								backgroundColor: '#333'
							}
						});
					}
				}).done(function(e) {
					$('body').unblock();
					var json = $.parseJSON(e);
					// console.log(json.response.url);
			        new PNotify({
			            title: json.response.status,
			            text: json.response.msg,
			            addclass: 'bg-'+json.response.status
			        });
					if (json.status) {
		    			setTimeout(function(){
		    				window.location.href = json.response.url;
		    			},2000);
					} else {
						return;
					}
				}).fail(function(e) {
					$('body').unblock();
					console.log(e);
				})
			});
		});
	</script>
	<script type="text/javascript" src="{{ asset('admin/js/pages/uploader_bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{asset('admin/js/pages/form_validation.js')}}"></script>
@endsection
