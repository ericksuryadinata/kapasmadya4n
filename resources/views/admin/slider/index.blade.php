@extends('layouts.admin')

@section('title')
Semua data {{str_replace('-',' ',Request::segment(2))}}
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Slider</span> - Semua data slider</h4>
		</div>

		<div class="heading-elements">
			{{-- <div class="heading-btn-group"> --}}
				<a href="{{route('admin.slider.create')}}" class="btn bg-teal-400 btn-labeled"><b><i class="icon-googleplus5"></i></b>Tambahkan {{Request::segment(2)}}</a>
			{{-- </div> --}}
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Slider</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
	<div class="panel panel-flat">

		<table class="table datatable-basic slider-table {{-- table-bordered --}}">
			<thead>
				<tr class="bg-slate">
					<th>No</th>
					<th>Image</th>
					<th class="text-center">Status</th>
					<th class="text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($models as $key => $model)
				<tr>
					<td>{{ $key+1 }}</td>
					<td>
						<img class="col-md-3 img-thumbnail" src="{{ $model->showImage() }}">
					</td>
					<td>
						<label class="label {{ ($model->status == 1) ? 'bg-primary' : 'bg-danger' }}">{{ ($model->status == 1) ? 'Active' : 'Non Active' }}</label>
					</td>
					<td class="text-center">
						<ul class="icons-list">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-menu9"></i>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="{{ route('admin.slider.edit', $model->id) }}"><i class="icon-pencil"></i> Edit</a></li>
									<li><a class="delete-modal" href="{{route('admin.slider.destroy', $model->id)}}"><i class="icon-trash"></i> Hapus</a></li>
								</ul>
							</li>
						</ul>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
    </div>
    <form id="form-delete" method="post" hidden>
        {{csrf_field()}} {{method_field('DELETE')}}
    </form>
@endsection

@section('min-js')
	<script type="text/javascript" src="{{ asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switch.min.js') }}"></script>
@endsection
@section('js')
	<script type="text/javascript" src="{{ asset('admin/js/pages/datatables_basic.js')}}"></script>
	{{-- <script type="text/javascript" src="{{ asset('admin/js/pages/form_checkboxes_radios.js') }}"></script> --}}
	<script type="text/javascript">
	    $(".switch").bootstrapSwitch();
        $(".slider-table").on('click', '.delete-modal', function (e) {
            e.preventDefault();
            let href = $(this).attr('href');
            $('#form-delete').attr('action', href);
            $('#form-delete').submit();
        });
	</script>
@endsection
