@extends('layouts.admin')

@section('title')
Pengaturan akun
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Pengaturan</span> - Akun Administrator</h4>
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="#">Pengaturan</a></li>
			<li class="active">Akun</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<h6 class="content-group text-semibold">
	<span class="text-primary"><i class="icon-user"></i> Pengaturan</span> Akun Administrator
	<small class="display-block">Mengatur akun administrator anda</small>
</h6>
<div class="row">
	<div class="col-lg-12">
		<div class="table tab-content-bordered">
			@include('admin.config.nav')
			<div class="tab-content">
				<div class="tab-pane active">
					<div style="padding: 20px">
						<form id="akun" method="post" action="{{ route('admin.user.update', Auth::id()) }}" class="form-horizontal">
							@method('PUT')
							@csrf
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<label class="col-md-2 control-label">Nama <b class="text-danger">*</b></label>
										<div class="col-md-10">
											<input type="name" name="name" placeholder="Nama Akun" class="form-control" value="{{ ucfirst($data->name) }}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Email <b class="text-danger">*</b></label>
										<div class="col-md-10">
											<input type="email" name="email" placeholder="ex: john@doe.com" class="form-control" value="{{ $data->email }}">
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="form-group">
										<label class="col-md-2 control-label">Username <b class="text-danger">*</b></label>
										<div class="col-md-10">
											<input type="name" name="username" placeholder="ex: john doe" class="form-control" value="{{ $data->username }}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Password <b class="text-danger">*</b></label>
										<div class="col-md-10">
											<div class="input-group">
												<input id="password" type="password" name="password" placeholder="password baru" class="form-control">
												<span id="show-password" class="input-group-addon"><i class="icon-eye2"></i></span>
											</div>
											<span class="text-danger">Nb: kosongkan jika password tidak ingin di ubah</span>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<button type="submit" class="btn btn-info pull-right">Perbarui</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	$("#akun").submit(function(e) {
		e.preventDefault();
		$.ajax({
			url: $("#akun").attr('action'),
			method: 'post',
			data: new FormData($("#akun")[0]),
			processData: false,
			contentType: false,
			beforeSend:function() {
				$('body').block({
					message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Sedang memperbarui data ... </span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 5,
						'-moz-border-radius': 5,
						backgroundColor: '#333'
					}
				});
			}
		}).done(function(data) {
			$('body').unblock();
			// console.log(data);
			var json = $.parseJSON(data);

			Swal.fire({
			  // position: 'center',
			  // width: 600,
			  type: ''+json.response.status+'',
			  title: json.response.status,
			  text: json.response.msg,
			  showConfirmButton: false,
			  timer: 2000
			});

			if (json.status) {
				setTimeout(function(){
					window.location.href = json.response.url;
				},2500);
			} else {
				return;
			}
		}).fail(function(data) {
			$('body').unblock();
			console.log(data);
		})
	});

	$("#show-password").on('click', function() {
		var input = $("#password");
		console.log(input);
		if (input.attr('type') === 'password') {
			input.attr('type', 'text');
		} else {
			input.attr('type', 'password');
		}
	})
</script>
@endsection