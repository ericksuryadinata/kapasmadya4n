@extends('layouts.admin')
@section('title')
Pengaturan website
@endsection
@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Pengaturan</span> - website</h4>
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="#">Pengaturan</a></li>
			<li class="active">Website</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<h6 class="content-group text-semibold">
	<span class="text-primary"><i class="icon-magazine"></i> Pengaturan</span> Tentang Website
	<small class="display-block">Mengatur Konten Tentang Website Anda</small>
</h6>
<div class="row">
	<div class="col-lg-12">
		<div class="table tab-content-bordered">
			@include('admin.config.nav')
			<div class="tab-content">
				<div class="tab-pane active">
					<div style="padding: 20px">
						<form id="config" class="form-horizontal" method="post" action="{{ route('admin.config.update', $config->id) }}">
							@csrf
							@method('PUT')
							<div class="form-group">
								<label class="col-lg-2 control-label">Name <b class="text-danger">*</b></label>
								<div class="col-lg-10">
									<input type="text" name="name" class="form-control" placeholder="Nama Website" value="{{ $config->name }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Alamat <b class="text-danger">*</b></label>
								<div class="col-lg-10">
									<input type="text" name="address" class="form-control" placeholder="Alamat" value="{{ $config->address }}">
									<input type="hidden" name="koordinate" value="{{ $config->koordinate }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Cari Lokasi <b class="text-danger">*</b></label>
								<div class="col-lg-10">
									<div class="form-group">
										<input id="gmap_loc" type="text" name="gmap_loc" class="form-control" placeholder="Cari lokasi anda" value="{{ $config->gmap_loc }}">
									</div>
									<div class="form-group">
										{{-- <div id="map" class="map-container map-basic"></div> --}}
										<div id="map" class="map-container map-basic"></div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Deskripsi <b class="text-danger">*</b></label>
								<div class="col-lg-10">
									<textarea id="editor-full" name="description">{!! $config->description !!}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-primary pull-right">Perbarui</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('min-js')
<script type="text/javascript" src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('admin/js/pages/editor_ckeditor.js') }}"></script>
<script type="text/javascript">

$("#config").submit(function(e) {
	e.preventDefault();
	var form = new FormData($("config")[0]);
	$.ajax({
		url: $("#config").attr('action'),
		method: 'post',
		data: new FormData($("#config")[0]),
		processData: false,
		contentType: false,
		beforeSend:function() {
			$('body').block({
				message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Sedang memperbarui data ... </span>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: '10px 15px',
					color: '#fff',
					width: 'auto',
					'-webkit-border-radius': 5,
					'-moz-border-radius': 5,
					backgroundColor: '#333'
				}
			});
		}
	}).done(function(data) {
		$('body').unblock();
		// console.log(data);
		var json = $.parseJSON(data);

		Swal.fire({
		  // position: 'center',
		  // width: 600,
		  type: ''+json.response.status+'',
		  title: json.response.status,
		  text: json.response.msg,
		  showConfirmButton: false,
		  timer: 2000
		});

		if (json.status) {
			setTimeout(function(){
				window.location.href = json.response.url;
			},2500);
		} else {
			return;
		}
	}).fail(function(data) {
		$('body').unblock();
		console.log(data);
	})
});
</script>
<script type="text/javascript">
function initMap() {
	var latlng 	= {lat: Number('{{ $config->getLatitude() }}'), lng: Number('{{ $config->getLongtitude() }}')};
	var map = new google.maps.Map(document.getElementById('map'), {
		center: latlng ,
		zoom: 18
	});

	var geocoder = new google.maps.Geocoder;
	var input = /** @type {!HTMLInputElement} */(
		document.getElementById('gmap_loc'));
	var autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.bindTo('bounds', map);

	var infowindow = new google.maps.InfoWindow();
	var marker = new google.maps.Marker({
		map: map,
		draggable: true,
		animation: google.maps.Animation.DROP,
		anchorPoint: new google.maps.Point(0, -29),
		position: {lat: Number('{{ $config->getLatitude() }}'), lng: Number('{{ $config->getLongtitude() }}')},
	});

	geocoder.geocode({'location': latlng}, function(results, status) {
		if (status === 'OK') {
			if (results[1]) {
				marker.setPosition(latlng);
				infowindow.setContent('<b>Memilih Area Ini</b><br>'+results[0].formatted_address);
				console.log(results);
				$("#gmap_loc").val(results[0].formatted_address);
				infowindow.open(map, marker);
			} else {
				infowindow.setContent('<b>Memilih Area Ini</b><br>');
				infowindow.open(map, marker);
			}
		} else {
			window.alert('Geocoder failed due to: ' + status);
		}
	});

	marker.addListener('dragend', function(evt){
		infowindow.close();
		marker.setVisible(false);

		marker.setVisible(true);

		var latlng = {lat: evt.latLng.lat(), lng: evt.latLng.lng()};
		geocoder.geocode({'location': latlng}, function(results, status) {
			if (status === 'OK') {
				if (results[1]) {
					marker.setPosition(latlng);
					infowindow.setContent('<b>Pilih Area Ini</b><br>'+results[0].formatted_address);
					infowindow.open(map, marker);
					$("#gmap_loc").val(results[0].formatted_address);
					$("input[name=koordinate]").val(evt.latLng.lat()+'|'+evt.latLng.lng());
				} else {
					infowindow.setContent('<b>Pilih Area Ini</b><br>');
					infowindow.open(map, marker);
					$("input[name=koordinate]").val(evt.latLng.lat()+'|'+evt.latLng.lng());
				}
			} else {
				window.alert('Geocoder failed due to: ' + status);
			}
		});
		/*infowindow.setContent('<div><strong>Pilih Area Ini</strong><br>'); infowindow.open(map, marker);*/
	});

	autocomplete.addListener('place_changed', function() {
		infowindow.close();
		marker.setVisible(false);
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			window.alert("Autocomplete's returned place contains no geometry"); return;
		}

		// If the place has a geometry, then present it on a map.
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17);  // Why 17? Because it looks good.
		}

		marker.setIcon(/** @type {google.maps.Icon} */({
			url: place.icon,
			size: new google.maps.Size(71, 71),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(17, 34),
			scaledSize: new google.maps.Size(35, 35)
		}));
		marker.setPosition(place.geometry.location);
		marker.setVisible(true);

		var address = '';
		if (place.address_components) {
			address = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || '')
			].join(' ');
		}

		$("input[name=koordinate]").val(place.geometry.location.lat()+'|'+place.geometry.location.lng());
		infowindow.setContent('<b>Pilih Area Ini</b><br>'+address);
		infowindow.open(map, marker);
	});
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBy_G0H8GDS521QaVAYPk_pinqKLqRdj3M&libraries=places&callback=initMap" async defer></script>
@endsection