@extends('layouts.admin')

@section('title')
Pengaturan seo website
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Pengaturan</span> - Seo website</h4>
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="#">Pengaturan</a></li>
			<li class="active">Seo</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<h6 class="content-group text-semibold">
	<span class="text-primary"><i class="icon-stats-growth"></i> Pengaturan</span> Seo Website
	<small class="display-block">Mengatur Seo website anda agar bisa terdeteksi google</small>
</h6>
<div class="row">
	<div class="col-lg-12">
		<div class="table tab-content-bordered">
			@include('admin.config.nav')
			<div class="tab-content">
				<div class="tab-panel active">
					<div style="padding: 20px">
						<form id="seo" class="form-horizontal" method="post" action="{{ route('admin.seo.update', $seo->id) }}">
							@csrf
							@method('PUT')
							<div class="form-group">
								<label class="col-md-2 control-label">Title <b class="text-danger">*</b></label>
								<div class="col-lg-10">
									<input type="text" name="title" class="form-control" placeholder="Title Headline" value="{{ $seo->title }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Author <b class="text-danger">*</b></label>
								<div class="col-lg-10">
									<input type="text" name="author" class="form-control" placeholder="Nama Author" value="{{ $seo->author }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">keyword <b class="text-danger">*</b></label>
								<div class="col-lg-10">
									<textarea class="form-control" name="keyword" placeholder="Keyword website">{{ $seo->keyword }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Tagline <b class="text-danger">*</b></label>
								<div class="col-lg-10">
									<textarea class="form-control" name="tagline" placeholder="Tagline website">{{ $seo->tagline }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Facebook pixel <br><span>( Opsional )</span></label>
								<div class="col-lg-10">
									<textarea class="form-control" name="fb_pixel" placeholder="Facebook Pixel">{{ $seo->fb_pixel }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Google Analytic <br><span>( Opsional )</span></label>
								<div class="col-lg-10">
									<textarea class="form-control" name="analytic" placeholder="Google analytic">{{ $seo->analytic }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-12">
									<button type="submit" class="btn btn-primary pull-right">Perbarui</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$("#seo").submit(function(e) {
	e.preventDefault();
	var form = new FormData($("#seo")[0]);
	$.ajax({
		url: $("#seo").attr('action'),
		method: 'post',
		data: new FormData($("#seo")[0]),
		processData: false,
		contentType: false,
		beforeSend:function() {
			$('body').block({
				message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Sedang memperbarui data ... </span>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: '10px 15px',
					color: '#fff',
					width: 'auto',
					'-webkit-border-radius': 5,
					'-moz-border-radius': 5,
					backgroundColor: '#333'
				}
			});
		}
	}).done(function(data) {
		$('body').unblock();
		// console.log(data);
		var json = $.parseJSON(data);

		Swal.fire({
		  // position: 'center',
		  // width: 600,
		  type: ''+json.response.status+'',
		  title: json.response.status,
		  text: json.response.msg,
		  showConfirmButton: false,
		  timer: 2000
		});

		if (json.status) {
			setTimeout(function(){
				window.location.href = json.response.url;
			},2500);
		} else {
			return;
		}
	}).fail(function(data) {
		$('body').unblock();
		console.log(data);
	})
});
</script>
@endsection