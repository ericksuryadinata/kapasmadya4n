
<ul class="nav nav-tabs nav-tabs-highlight">
	<li @if (Request::segment(2) == 'config') class="active" @endif>
		<a href="{{ route('admin.config.edit', $config->id) }}"><i class="icon-magazine position-left"></i> Tentang Website</a>
	</li>
	<li @if (Request::segment(2) == 'seo') class="active" @endif>
		<a href="{{ route('admin.seo.edit', $seo->id) }}"><i class="icon-stats-growth position-left"></i> Seo Website</a>
	</li>
	<li @if (Request::segment(2) == 'contact') class="active" @endif>
		<a href="{{ route('admin.contact.edit', $config->id) }}"><i class="icon-address-book2 position-left"></i> Informasi kontak</a>
	</li>
	<li @if (Request::segment(2) == 'logo') class="active" @endif>
		<a href="{{ route('admin.logo.edit', $config->id) }}"><i class="icon-image4 position-left"></i> Logo</a>
	</li>
	<li @if (Request::segment(2) == 'user') class="active" @endif>
		<a href="{{ route('admin.user.edit', Auth::id()) }}"><i class="icon-user position-left"></i> Akun</a>
	</li>
</ul>
