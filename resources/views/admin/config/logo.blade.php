@extends('layouts.admin')

@section('title')
Pengaturan logo website
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Pengaturan</span> - logo website</h4>
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="#">Pengaturan</a></li>
			<li class="active">logo</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<h6 class="content-group text-semibold">
	<span class="text-primary"><i class="icon-magazine"></i> Pengaturan</span> Logo Website
	<small class="display-block">Mengatur logo website anda</small>
</h6>

<div class="row">
	<div class="col-lg-12">
		<div class="table tab-content-bordered">
			@include('admin.config.nav')
			<div class="tab-content">
				<div class="tab-pane active">
					<div style="padding: 20px">
						<form id="logo" class="form-horizontal" action="{{ route('admin.logo.update', $config->id) }}" method="post" enctype="multipart/form-data">
							@csrf
							@method('PUT')
							<div class="form-group">
								<label class="control-label col-md-2">Logo <b class="text-danger">*</b></label>
								<div class="col-md-10">
									<input type="file" class="file-input-custom" name="logo">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Icon <b class="text-danger">*</b></label>
								<div class="col-md-10">
									<input type="file" name="icon" class="file-input-custom">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Logo White <b class="text-danger">*</b></label>
								<div class="col-md-10">
									<input type="file" name="logo_white" class="file-input-custom">
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-info">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('min-js')
<script type="text/javascript" src="{{ asset('admin/js/plugins/uploaders/fileinput.min.js') }}"></script>
@endsection

@section('js')
{{-- <script type="text/javascript" src="{{ asset('admin/js/pages/uploader_bootstrap.js') }}"></script> --}}
<script type="text/javascript">

$("#logo").submit(function(e) {
	e.preventDefault();
	var form = new FormData($("config")[0]);
	$.ajax({
		url: $("#logo").attr('action'),
		method: 'post',
		data: new FormData($("#logo")[0]),
		processData: false,
		contentType: false,
		beforeSend:function() {
			$('body').block({
				message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Sedang memperbarui data ... </span>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: '10px 15px',
					color: '#fff',
					width: 'auto',
					'-webkit-border-radius': 5,
					'-moz-border-radius': 5,
					backgroundColor: '#333'
				}
			});
		}
	}).done(function(data) {
		$('body').unblock();
		// console.log(data);
		var json = $.parseJSON(data);

		Swal.fire({
		  // position: 'center',
		  // width: 600,
		  type: ''+json.response.status+'',
		  title: json.response.status,
		  text: json.response.msg,
		  showConfirmButton: false,
		  timer: 2000
		});

		if (json.status) {
			setTimeout(function(){
				window.location.href = json.response.url;
			},2500);
		} else {
			return;
		}
	}).fail(function(data) {
		$('body').unblock();
		console.log(data);
	})
});
	$("input[name=logo]").fileinput({
    	previewFileType: 'image',
        browseLabel: 'Select',
        browseClass: 'btn bg-slate-700',
        browseIcon: '<i class="icon-image2 position-left"></i> ',
        removeLabel: 'Remove',
        removeClass: 'btn btn-danger',
        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
        uploadClass: 'hidden',
        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
        layoutTemplates: {
            // caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
        },
        initialPreview: ["<img src='{{ asset('storage/'.$logo->logo) }}' class='file-preview-image' alt=''>",],
        overwriteInitial: true,
	});

	$("input[name=icon]").fileinput({
    	previewFileType: 'image',
        browseLabel: 'Select',
        browseClass: 'btn bg-slate-700',
        browseIcon: '<i class="icon-image2 position-left"></i> ',
        removeLabel: 'Remove',
        removeClass: 'btn btn-danger',
        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
        uploadClass: 'hidden',
        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
        layoutTemplates: {
            // caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
        },
        initialPreview: ["<img src='{{ asset('storage/'.$logo->icon) }}' class='file-preview-image' alt=''>",],
        overwriteInitial: true,
	});

	$("input[name=logo_white]").fileinput({
    	previewFileType: 'image',
        browseLabel: 'Select',
        browseClass: 'btn bg-slate-700',
        browseIcon: '<i class="icon-image2 position-left"></i> ',
        removeLabel: 'Remove',
        removeClass: 'btn btn-danger',
        removeIcon: '<i class="icon-cancel-square position-left"></i> ',
        uploadClass: 'hidden',
        uploadIcon: '<i class="icon-file-upload position-left"></i> ',
        layoutTemplates: {
            // caption: '<div tabindex="-1" class="form-control file-caption {class}">\n' + '<span class="icon-file-plus kv-caption-icon"></span><div class="file-caption-name"></div>\n' + '</div>'
        },
        initialPreview: ["<img src='{{ asset('storage/'.$logo->logo_white) }}' class='file-preview-image' alt=''>",],
        overwriteInitial: true,
	});
</script>
@endsection
