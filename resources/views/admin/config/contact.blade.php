@extends('layouts.admin')

@section('title')
Pengaturan contact website
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Pengaturan</span> - Kontak website</h4>
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="#">Pengaturan</a></li>
			<li class="active">kontak</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<h6 class="content-group text-semibold">
	<span class="text-primary"><i class="icon-magazine"></i> Pengaturan</span> Kontak Website
	<small class="display-block">Mengatur kontak website anda</small>
</h6>
<div class="row">
	<div class="col-lg-12">
		<div class="table tab-content-bordered">
			@include('admin.config.nav')
			<div class="tab-content">
				<div class="tab-pane active">
					<div style="padding: 20px">
						<form id="contact" method="post" action="{{ route('admin.contact.update', $config->id) }}" class="form-horizontal">
							@csrf
							@method('PUT')
							<div class="form-group">
								<label class="control-label col-md-2">Email <b class="text-danger">*</b></label>
								<div class="col-md-10">
									<input type="email" name="email" class="form-control" placeholder="Email anda" value="{{ $contact->email }}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Phone <b class="text-danger">*</b></label>
								<div class="col-md-10">
									<input type="text" name="phone" class="form-control" placeholder="No Tephone anda" value="{{ $contact->phone }}">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Fax <b class="text-danger">*</b><span>( Opsional )</span></label>
								<div class="col-md-10">
									<input type="text" name="fax" class="form-control" placeholder="Fax. ex:031193XXX" value="{{ $contact->fax }}">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary pull-right">Perbarui</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	$("#contact").submit(function(e) {
		e.preventDefault();
		$.ajax({
			url: $("#contact").attr('action'),
			method: 'post',
			data: new FormData($("#contact")[0]),
			processData: false,
			contentType: false,
			beforeSend:function() {
				$('body').block({
					message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Sedang memperbarui data ... </span>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: '10px 15px',
						color: '#fff',
						width: 'auto',
						'-webkit-border-radius': 5,
						'-moz-border-radius': 5,
						backgroundColor: '#333'
					}
				});
			}
		}).done(function(data) {
			$('body').unblock();
			// console.log(data);
			var json = $.parseJSON(data);

			Swal.fire({
			  // position: 'center',
			  // width: 600,
			  type: ''+json.response.status+'',
			  title: json.response.status,
			  text: json.response.msg,
			  showConfirmButton: false,
			  timer: 2000
			});

			if (json.status) {
				setTimeout(function(){
					window.location.href = json.response.url;
				},2500);
			} else {
				return;
			}
		}).fail(function(data) {
			$('body').unblock();
			console.log(data);
		})
	});
</script>
@endsection