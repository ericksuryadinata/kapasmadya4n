@extends('layouts.admin')

@section('title')
Buat gallery baru
@endsection

@section('heading')
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><span class="text-semibold">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</span> - Buat
                {{ucwords(str_replace('-',' ',Request::segment(2)))}} baru</h4>
		</div>

		<div class="heading-elements">
			{{-- <div class="heading-btn-group"> --}}
				<a href="{{route('admin.gallery.index')}}" class="btn bg-teal-400 btn-labeled"><b><i class="icon-square-left"></i></b>Kembali</a>
			{{-- </div> --}}
		</div>
	</div>

	<div class="breadcrumb-line breadcrumb-line-component">
		<ul class="breadcrumb">
			<li><a href="{{ route('admin.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{ route('admin.gallery.index') }}">{{ucwords(str_replace('-',' ',Request::segment(2)))}}</a></li>
			<li class="active">Buat</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<form class="form-horizontal" method="post" action="{{ route('admin.gallery.store') }}" id="formdata" enctype="multipart/form-data">
	@csrf
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-flat" style="min-height: 340px">
				<div class="panel-body">
					<div class="col-md-12">
                        <div id="image">
                            <div class="form-group">
                                <label>Masukkan Gambar sebagai banner</label>
                                <input type="file" id="fileImage" name="image" class="file-input" accept="image/*" data-show-upload="false">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Status <b class="text-danger">*</b></label><br>
                            <input type="checkbox" name="status" class="switch" data-on-text='Aktif' data-off-text='Non-Aktif'>
                        </div>
						<div class="form-group">
							<label>Nama <b class="text-danger">*</b></label>
							<input type="text" name="name" class="form-control" placeholder="John Doe">
						</div>
						<div class="form-group">
							<label>Description <b class="text-danger">*</b></label>
							<textarea class="form-control" name="description" id="description" rows="6" placeholder="Deskripsi galleri max 200 karakter" maxlength="201"></textarea>
							<span id="notify" class="text-danger" style="display: none">Batas karakter sudah mencapai 200</span>
							<span id='counter' class="pull-right text-justify"></span>
						</div>
						<div class="col-sm-12 col-md-12">
							<label>Dokumentasi Kegiatan Warga</label>
							<div class="dropzone" id="frmFileUpload">

							</div>
						</div>
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-primary pull-right">Buat Gallery<i class="icon-arrow-right14 position-right"></i></button>
		</div>
	</div>
</form>
@endsection

@section('min-js')
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/validation/validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/js/plugins/uploaders/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/uploaders/dropzone.min.js') }}"></script>
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('admin/js/pages/form_validation.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#formdata').submit(function(e) {
				e.preventDefault();
				$.ajax({
					url: $(this).attr('action'),
					data: new FormData(this),
					method: 'post',
		            contentType: false,
		            processData: false,
					beforeSend:function(){
						$('body').block({
							message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; Data sedang di proses ... </span>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: '10px 15px',
								color: '#fff',
								width: 'auto',
								'-webkit-border-radius': 5,
								'-moz-border-radius': 5,
								backgroundColor: '#333'
							}
						});
					}
				}).done(function(e) {
					$('body').unblock();
					var json = $.parseJSON(e);
					// console.log(json.response.url);
			        new PNotify({
			            title: json.response.status,
			            text: json.response.msg,
			            addclass: 'bg-'+json.response.status
			        });
					if (json.status) {
		    			setTimeout(function(){
		    				window.location.href = json.response.url;
		    			},2500);
					} else {
						return;
					}
				}).fail(function(e) {
					$('body').unblock();
					console.log(e);
				})
			});

            $("#description").on('input propertychange', function() {
			    if (this.value.length >= 201) {
                    $("#notify").fadeIn();
                } else {
                    $("#notify").fadeOut();
                    $("#counter").html(this.value.length);
                }
            });
		});

		Dropzone.options.frmFileUpload = {
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			paramName: 'file',
            dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
			url: "{{ route('admin.gallery.store.images') }}",
			maxFilesize: 12,
			acceptedFiles: ".jpeg,.jpg,.png,.gif",
			addRemoveLinks: true,
			timeout: 5000,
            maxFilesize: 2,
			parallelUploads:1,
			init:function(){
				// Add server images
				var myDropzone = this;
				$.ajax({
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
					type: 'GET',
					url: '{{ route("admin.gallery.get.images") }}',
				}).done(function(data){
					console.log(data);
					let public_path = '{{ url("storage") }}';
					$.each(data.images, function (key, value) {
						var file = {name: value.original, size: value.size};
						myDropzone.options.addedfile.call(myDropzone, file);
						myDropzone.options.thumbnail.call(myDropzone, file, public_path+"/"+value.server);
						let fileuploaded = file.previewElement.querySelector("[data-dz-name]");
						// append the response key to create data-key
						fileuploaded.dataset.key = value.key;
						fileuploaded.innerHTML = value.name;
						myDropzone.emit("complete", file);
					});
				}).error(function(data){
					Swal.fire('Error', 'error getting data, please refresh the page','error');
				});

				this.on("removedfile", function(file, response){
					let dz_name = file.previewElement.querySelector("[data-dz-name]");
					// take the data-key
					let key = dz_name.dataset.key;
					$.ajax({
						headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
						type: 'POST',
						url: '{{ route("admin.gallery.delete.images") }}',
						data: { filekey: key },
						success: function (data){
                            new PNotify({
                                title: 'success',
                                text: 'File has been successfully removed!!',
                                addclass: 'bg-success'
                            });
						},
						error: function(e) {
                            console.log(e);
                            new PNotify({
                                title: 'error',
                                text: 'File not removed',
                                addclass: 'bg-danger'
                            });
						}
					});
					let fileRef;
					return (fileRef = file.previewElement) != null ?
					fileRef.parentNode.removeChild(file.previewElement) : void 0;
				});
			},
			success: function(file, response)
			{
				let fileuploaded = file.previewElement.querySelector("[data-dz-name]");
				// append the response key to create data-key
				fileuploaded.dataset.key = response.key;
				fileuploaded.innerHTML = response.name;
                new PNotify({
                    title: 'success',
                    text: 'Upload Success',
                    addclass: 'bg-success'
                });
			},
			error: function(file, response)
			{
                let fileRef;
                return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
                new PNotify({
                    title: 'error',
                    text: 'File not uploaded',
                    addclass: 'bg-danger'
                });
			}
        };

        var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
            '  <div class="modal-content">\n' +
            '    <div class="modal-header">\n' +
            '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
            '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
            '    </div>\n' +
            '    <div class="modal-body">\n' +
            '      <div class="floating-buttons btn-group"></div>\n' +
            '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
            '    </div>\n' +
            '  </div>\n' +
            '</div>\n';

        var previewZoomButtonClasses = {
            toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
            fullscreen: 'btn btn-default btn-icon btn-xs',
            borderless: 'btn btn-default btn-icon btn-xs',
            close: 'btn btn-default btn-icon btn-xs'
        };

        var previewZoomButtonIcons = {
            prev: '<i class="icon-arrow-left32"></i>',
            next: '<i class="icon-arrow-right32"></i>',
            toggleheader: '<i class="icon-menu-open"></i>',
            fullscreen: '<i class="icon-screen-full"></i>',
            borderless: '<i class="icon-alignment-unalign"></i>',
            close: '<i class="icon-cross3"></i>'
        };

        var fileActionSettings = {
            zoomClass: 'btn btn-link btn-xs btn-icon',
            zoomIcon: '<i class="icon-zoomin3"></i>',
            dragClass: 'btn btn-link btn-xs btn-icon',
            dragIcon: '<i class="icon-three-bars"></i>',
            removeClass: 'btn btn-link btn-icon btn-xs',
            removeIcon: '<i class="icon-trash"></i>',
            indicatorNew: '',
            indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
            indicatorError: '<i class="icon-cross2 text-danger"></i>',
            indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
        };

        $('.file-input').fileinput({
            browseLabel: 'Browse',
            browseIcon: '<i class="icon-file-plus"></i>',
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                modal: modalTemplate
            },
            initialCaption: "No file selected",
            previewZoomButtonClasses: previewZoomButtonClasses,
            previewZoomButtonIcons: previewZoomButtonIcons,
            fileActionSettings: fileActionSettings
        });
	</script>
@endsection
