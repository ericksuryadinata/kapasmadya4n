<?php

use Illuminate\Database\Seeder;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('configs')->truncate();

        \App\Models\Config::create([
        	'name' => 'Your Website Name',
        	'logo' => null,
        	'logo_white' => null,
        	'icon' => null,
        	'phone' => '08123456789',
        	'email' => 'email@yourwebsite.com',
        	'fax' => '08123456789',
        	'address' => 'Your Website Address',
        	'description' => 'Your Website Description',
            'gmap_loc' => 'Your Website Address',
            'koordinate' => '1287368123|128371203'
        ]);
    }
}
