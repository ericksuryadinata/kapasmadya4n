<?php

use Illuminate\Database\Seeder;

class SeosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('seos')->truncate();

        \App\Models\Seo::create([
        	'title' => 'Your Website Title',
        	'author' => 'Your Website Author',
        	'keyword' => 'Your Website Keyword',
            'tagline' => 'Your Website Tagline',
        	'fb_pixel' => null,
        	'analytic' => null,
        ]);
    }
}
