<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->truncate();

        \App\User::create([
        	'name' => 'administrator',
            'username' => 'admin',
        	'email' => 'admin@yourwebsite.com',
        	'email_verified_at' => null,
        	'password' => bcrypt('bismillah')
        ]);
    }
}
