function notify(json) {
    new PNotify({
        title: json.response.status,
        text: json.response.msg,
        addclass: 'bg-'+json.response.status
    });
}

function blockMessage(message, html='body') {
	$(html).block({
		message: '<span class="text-semibold"><i class="icon-spinner9 spinner position-left"></i>&nbsp; '+message+' ... </span>',
		overlayCSS: {
			backgroundColor: '#fff',
			opacity: 0.8,
			cursor: 'wait'
		},
		css: {
			border: 0,
			padding: '10px 15px',
			color: '#fff',
			width: 'auto',
			'-webkit-border-radius': 5,
			'-moz-border-radius': 5,
			backgroundColor: '#333'
		}
	});
}

$(".show-password").onclick(function() {
	if (this.type === 'password') {
		this.type = 'text';
	} else {
		this.type = 'password';
	}
})