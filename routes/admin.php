<?php

/*
 * Routing khusus admin panel
 */
Route::group(['prefix' => 'administrator', 'middleware' => ['globalvar', 'admin.panel'], 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::get('kegiatan-warga/get/images', 'Gallery\GalleryController@getImages')->name('gallery.get.images');
    Route::post('kegiatan-warga/store/images', 'Gallery\GalleryController@storeImages')->name('gallery.store.images');
    Route::post('kegiatan-warga/delete/images', 'Gallery\GalleryController@deleteImages')->name('gallery.delete.images');
    Route::resource('/', 'Dashboard\DashboardController');
    Route::resource('config', 'Config\ConfigController');
    Route::resource('seo', 'Config\SeoController');
    Route::resource('contact', 'Config\ContactController');
    Route::resource('logo', 'Config\LogoController');
    Route::resource('user', 'Config\UserController');
    Route::resource('slider', 'Slider\SliderController');
    Route::resource('kontak', 'ContactUs\ContactUsController')->names([
        'index' => 'contactus.index',
        'create' => 'contactus.create',
        'store' => 'contactus.store',
        'show' => 'contactus.show',
        'edit' => 'contactus.edit',
        'update' => 'contactus.update',
        'destroy' => 'contactus.destroy',
    ])->parameters(['kontak' => 'contactus']);
    Route::resource('produk-warga', 'Merchandise\MerchandiseController')->names([
        'index' => 'merchandise.index',
        'create' => 'merchandise.create',
        'store' => 'merchandise.store',
        'show' => 'merchandise.show',
        'edit' => 'merchandise.edit',
        'update' => 'merchandise.update',
        'destroy' => 'merchandise.destroy',
    ])->parameters(['produk-warga' => 'merchandise']);
    Route::resource('kegiatan-warga', 'Gallery\GalleryController')->names([
        'index' => 'gallery.index',
        'create' => 'gallery.create',
        'store' => 'gallery.store',
        'show' => 'gallery.show',
        'edit' => 'gallery.edit',
        'update' => 'gallery.update',
        'destroy' => 'gallery.destroy',
    ])->parameters(['kegiatan-warga' => 'gallery']);
    Route::resource('berita', 'News\NewsController')->names([
        'index' => 'news.index',
        'create' => 'news.create',
        'store' => 'news.store',
        'show' => 'news.show',
        'edit' => 'news.edit',
        'update' => 'news.update',
        'destroy' => 'news.destroy',
    ])->parameters(['berita' => 'news']);

    Route::resource('informasi', 'Article\ArticleController')->names([
        'index' => 'article.index',
        'create' => 'article.create',
        'store' => 'article.store',
        'show' => 'article.show',
        'edit' => 'article.edit',
        'update' => 'article.update',
        'destroy' => 'article.destroy',
    ])->parameters(['informasi' => 'article']);
    Route::get('informasi/image/get',     'Article\ImageController@getImage')->name('article.get_image');
    Route::post('informasi/image/store',  'Article\ImageController@storeImage')->name('article.store_image');
    Route::post('informasi/image/delete', 'Article\ImageController@deleteImage')->name('article.delete_image');

    Route::get('signout', 'AuthController@signout')->name('signout');
});

Route::group(['prefix' => 'auth', 'namespace' => 'Admin'], function () {
    Route::get('admin', 'AuthController@index')->name('login.admin');
    Route::post('admin', 'AuthController@store')->name('auth.admin');
});
