<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Frontend', 'as' => 'frontend.', 'middleware' => ['globalvar']], function() {
    Route::get('/', 'Home\HomeController@index')->name('home');
    Route::get('berita', 'News\NewsController@index')->name('news');
    Route::get('berita/{slug}', 'News\NewsController@detail')->name('news.detail');
    Route::get('kegiatan-warga', 'Event\EventController@index')->name('activities');
    Route::get('kegiatan-warga/{slug}', 'Event\EventController@detail')->name('activities.detail');
    Route::get('kontak', 'Contact\ContactController@index')->name('contact');
    Route::get('produk-warga', 'Merchandise\MerchandiseController@index')->name('merchandise');
    Route::get('produk-warga/{slug}', 'Merchandise\MerchandiseController@detail')->name('merchandise.detail');
    Route::get('informasi', 'Information\InformationController@index')->name('informasi');
    Route::get('informasi/{id}', 'Information\InformationController@show')->name('informasi.detail');
    Route::get('cache', function() {
        $artisan = Artisan::call('view:cache');
    	 // $artisan = Artisan::call('migrate');

    	$artisan = Artisan::call('view:clear');
    	echo "Done";
    });
});
